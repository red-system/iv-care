var wc_add_to_cart_params = {
    "ajax_url": "/",
    "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
    "i18n_view_cart": "View cart",
    "cart_url": "http:/",
    "is_cart": "",
    "cart_redirect_after_add": "no"
};
var booked_js_vars = {
	"ajax_url": "http:\/\/dentario.themerex.net\/wp-admin\/admin-ajax.php",
	"profilePage": "",
	"publicAppointments": "",
	"i18n_confirm_appt_delete": "Are you sure you want to cancel this appointment?",
	"i18n_please_wait": "Please wait ...",
	"i18n_wrong_username_pass": "Wrong username\/password combination.",
	"i18n_fill_out_required_fields": "Please fill out all required fields.",
	"i18n_guest_appt_required_fields": "Please enter your name to book an appointment.",
	"i18n_appt_required_fields": "Please enter your name, your email address and choose a password to book an appointment.",
	"i18n_appt_required_fields_guest": "Please fill in all \"Information\" fields.",
	"i18n_password_reset": "Please check your email for instructions on resetting your password.",
	"i18n_password_reset_error": "That username or email is not recognized."
};
var woocommerce_params = {
	"ajax_url": "\/wp-admin\/admin-ajax.php",
	"wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
};
var wc_cart_fragments_params = {
	"ajax_url": "\/wp-admin\/admin-ajax.php",
	"wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
	"fragment_name": "wc_fragments"
};
var mejsL10n = {
	"language": "en-US",
	"strings": {
		"Close": "Close",
		"Fullscreen": "Fullscreen",
		"Turn off Fullscreen": "Turn off Fullscreen",
		"Go Fullscreen": "Go Fullscreen",
		"Download File": "Download File",
		"Download Video": "Download Video",
		"Play": "Play",
		"Pause": "Pause",
		"Captions\/Subtitles": "Captions\/Subtitles",
		"None": "None",
		"Time Slider": "Time Slider",
		"Skip back %1 seconds": "Skip back %1 seconds",
		"Video Player": "Video Player",
		"Audio Player": "Audio Player",
		"Volume Slider": "Volume Slider",
		"Mute Toggle": "Mute Toggle",
		"Unmute": "Unmute",
		"Mute": "Mute",
		"Use Up\/Down Arrow keys to increase or decrease volume.": "Use Up\/Down Arrow keys to increase or decrease volume.",
		"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."
	}
};
var _wpmejsSettings = {
	"pluginPath": "\/wp-includes\/js\/mediaelement\/"
};
		
if (typeof DENTARIO_STORAGE == 'undefined') var DENTARIO_STORAGE = {};
	DENTARIO_STORAGE["strings"] = {
		ajax_error: "Invalid server answer",
		bookmark_add: "Add the bookmark",
		bookmark_added: "Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab &#039;Bookmarks&#039;",
		bookmark_del: "Delete this bookmark",
		bookmark_title: "Enter bookmark title",
		bookmark_exists: "Current page already exists in the bookmarks list",
		search_error: "Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.",
		email_confirm: "On the e-mail address &quot;%s&quot; we sent a confirmation email. Please, open it and click on the link.",
		reviews_vote: "Thanks for your vote! New average rating is:",
		reviews_error: "Error saving your vote! Please, try again later.",
		error_like: "Error saving your like! Please, try again later.",
		error_global: "Global error text",
		name_empty: "The name can&#039;t be empty",
		name_long: "Too long name",
		email_empty: "Too short (or empty) email address",
		email_long: "Too long email address",
		email_not_valid: "Invalid email address",
		subject_empty: "The subject can&#039;t be empty",
		subject_long: "Too long subject",
		text_empty: "The message text can&#039;t be empty",
		text_long: "Too long message text",
		send_complete: "Send message complete!",
		send_error: "Transmit failed!",
		login_empty: "The Login field can&#039;t be empty",
		login_long: "Too long login field",
		login_success: "Login success! The page will be reloaded in 3 sec.",
		login_failed: "Login failed!",
		password_empty: "The password can&#039;t be empty and shorter then 4 characters",
		password_long: "Too long password",
		password_not_equal: "The passwords in both fields are not equal",
		registration_success: "Registration success! Please log in!",
		registration_failed: "Registration failed!",
		geocode_error: "Geocode was not successful for the following reason:",
		googlemap_not_avail: "Google map API not available!",
		editor_save_success: "Post content saved!",
		editor_save_error: "Error saving post data!",
		editor_delete_post: "You really want to delete the current post?",
		editor_delete_post_header: "Delete post",
		editor_delete_success: "Post deleted!",
		editor_delete_error: "Error deleting post!",
		editor_caption_cancel: "Cancel",
		editor_caption_close: "Close"
	};

if (typeof DENTARIO_STORAGE == 'undefined') var DENTARIO_STORAGE = {};
	DENTARIO_STORAGE['ajax_url'] = 'http://dentario.themerex.net/wp-admin/admin-ajax.php';
	DENTARIO_STORAGE['ajax_nonce'] = '178c041120';
	DENTARIO_STORAGE['site_url'] = 'http://dentario.themerex.net';
	DENTARIO_STORAGE['vc_edit_mode'] = false;
	DENTARIO_STORAGE['theme_font'] = 'Poppins';
	DENTARIO_STORAGE['theme_skin'] = 'less';
	DENTARIO_STORAGE['theme_skin_color'] = '#232A34';
	DENTARIO_STORAGE['theme_skin_bg_color'] = '#ffffff';
	DENTARIO_STORAGE['slider_height'] = 517;
	DENTARIO_STORAGE['system_message'] = {
		message: '',
		status: '',
		header: ''
	};
	DENTARIO_STORAGE['user_logged_in'] = false;
	DENTARIO_STORAGE['toc_menu'] = 'float';
	DENTARIO_STORAGE['toc_menu_home'] = true;
	DENTARIO_STORAGE['toc_menu_top'] = true;
	DENTARIO_STORAGE['menu_fixed'] = true;
	DENTARIO_STORAGE['menu_mobile'] = 1023;
	DENTARIO_STORAGE['menu_slider'] = true;
	DENTARIO_STORAGE['menu_cache'] = false;
	DENTARIO_STORAGE['demo_time'] = 0;
	DENTARIO_STORAGE['media_elements_enabled'] = true;
	DENTARIO_STORAGE['ajax_search_enabled'] = true;
	DENTARIO_STORAGE['ajax_search_min_length'] = 3;
	DENTARIO_STORAGE['ajax_search_delay'] = 200;
	DENTARIO_STORAGE['css_animation'] = true;
	DENTARIO_STORAGE['menu_animation_in'] = 'fadeIn';
	DENTARIO_STORAGE['menu_animation_out'] = 'fadeOutDown';
	DENTARIO_STORAGE['popup_engine'] = 'magnific';
	DENTARIO_STORAGE['email_mask'] = '^([a-zA-Z0-9_\-]+\.)*[a-zA-Z0-9_\-]+@[a-z0-9_\-]+(\.[a-z0-9_\-]+)*\.[a-z]{2,6}$';
	DENTARIO_STORAGE['contacts_maxlength'] = 1000;
	DENTARIO_STORAGE['comments_maxlength'] = 1000;
	DENTARIO_STORAGE['remember_visitors_settings'] = false;
	DENTARIO_STORAGE['admin_mode'] = false;
	DENTARIO_STORAGE['isotope_resize_delta'] = 0.3;
	DENTARIO_STORAGE['error_message_box'] = null;
	DENTARIO_STORAGE['viewmore_busy'] = false;
	DENTARIO_STORAGE['video_resize_inited'] = false;
	DENTARIO_STORAGE['top_panel_height'] = 0;
	

jQuery("document").ready(function() {
	"use strict";
	DENTARIO_STORAGE['phone_mask'] = '^[0-9\-\+]{9,15}$';
	DENTARIO_STORAGE["strings"]["phone_not_valid"] = "Invalid phone number";
	DENTARIO_STORAGE["strings"]["phone_empty"] = "The phone can not be empty";
	DENTARIO_STORAGE["strings"]["phone_wrong"] = "The phone is wrong";
	DENTARIO_STORAGE["strings"]["doctor_empty"] = "Doctor name can not be empty";
	DENTARIO_STORAGE["strings"]["doctor_long"] = "Doctor name too long";
});

var wc_single_product_params = {
	"i18n_required_rating_text": "Please select a rating",
	"review_rating_required": "yes",
	"flexslider": {
		"rtl": false,
		"animation": "slide",
		"smoothHeight": false,
		"directionNav": false,
		"controlNav": "thumbnails",
		"slideshow": false,
		"animationSpeed": 500,
		"animationLoop": false
	},
	"zoom_enabled": "",
	"photoswipe_enabled": "",
	"flexslider_enabled": ""
};

var tribe_l10n_datatables = {
	"aria": {
		"sort_ascending": ": activate to sort column ascending",
		"sort_descending": ": activate to sort column descending"
	},
	"length_menu": "Show _MENU_ entries",
	"empty_table": "No data available in table",
	"info": "Showing _START_ to _END_ of _TOTAL_ entries",
	"info_empty": "Showing 0 to 0 of 0 entries",
	"info_filtered": "(filtered from _MAX_ total entries)",
	"zero_records": "No matching records found",
	"search": "Search:",
	"pagination": {
		"all": "All",
		"next": "Next",
		"previous": "Previous"
	},
	"select": {
		"rows": {
			"0": "",
			"_": ": Selected %d rows",
			"1": ": Selected 1 row"
		}
	},
	"datepicker": {
		"dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		"dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
		"dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
		"monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		"monthNamesShort": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		"nextText": "Next",
		"prevText": "Prev",
		"currentText": "Today",
		"closeText": "Done"
	}
};

if (typeof DENTARIO_STORAGE == 'undefined') var DENTARIO_STORAGE = {};
if (DENTARIO_STORAGE['theme_font'] == '') DENTARIO_STORAGE['theme_font'] = 'Poppins';
DENTARIO_STORAGE['theme_skin_color'] = '#232A34';
DENTARIO_STORAGE['theme_skin_bg_color'] = '#ffffff';



jQuery(document).ready(function() {
	"use strict";
    if (jQuery(".rev_slider").length > 0) {
        initRevSlider()
    };

    if (jQuery("#tribe-events").length > 0) {
        initEvents()
    };
    if (jQuery(".isotope_filters").length > 0) {
        initFilters()
    };
});


function initRevSlider() {
	"use strict";

    var ajaxRevslider = function(obj) {
		"use strict";

        // obj.type : Post Type
        // obj.id : ID of Content to Load
        // obj.aspectratio : The Aspect Ratio of the Container / Media
        // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

        var content = "";

        data = {};

        data.action = 'revslider_ajax_call_front';
        data.client_action = 'get_slider_html';
        data.token = '0dd99bc0ed';
        data.type = obj.type;
        data.id = obj.id;
        data.aspectratio = obj.aspectratio;

        // SYNC AJAX REQUEST
        jQuery.ajax({
            type: "post",
            url: "http://dentario.themerex.net/wp-admin/admin-ajax.php",
            dataType: 'json',
            data: data,
            async: false,
            success: function(ret, textStatus, XMLHttpRequest) {
                if (ret.success == true)
                    content = ret.data;
            },
            error: function(e) {
                console.log(e);
            }
        });

        // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
        return content;
    };

    // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
    var ajaxRemoveRevslider = function(obj) {
        return jQuery(obj.selector + " .rev_slider").revkill();
    };

    // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
    var extendessential = setInterval(function() {
        if (jQuery.fn.tpessential != undefined) {
            clearInterval(extendessential);
            if (typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                jQuery.fn.tpessential.defaults.ajaxTypes.push({
                    type: "revslider",
                    func: ajaxRevslider,
                    killfunc: ajaxRemoveRevslider,
                    openAnimationSpeed: 0.3
                });
                // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                // func: the Function Name which is Called once the Item with the Post Type has been clicked
                // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
            }
        }
    }, 30);

    function setREVStartSize(e) {
		"use strict";
        try {
            var i = jQuery(window).width(),
                t = 9999,
                r = 0,
                n = 0,
                l = 0,
                f = 0,
                s = 0,
                h = 0;
            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                var u = (e.c.width(), jQuery(window).height());
                if (void 0 != e.fullScreenOffsetContainer) {
                    var c = e.fullScreenOffsetContainer.split(",");
                    if (c) jQuery.each(c, function(e, i) {
                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                }
                f = u
            } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
            e.c.closest(".rev_slider_wrapper").css({
                height: f
            })
        } catch (d) {
            console.log("Failure at Presize of Slider:" + d)
        }
    };

    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = "";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    }

    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = ".tp-caption.dentrarario-home1-static-header,.dentrarario-home1-static-header{color:rgba(48,56,59,1.00);font-size:42px;line-height:48px;font-weight:300;font-style:normal;font-family:Open Sans;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.dentrarario-home1-static-text,.dentrarario-home1-static-text{color:rgba(110,120,124,1.00);font-size:16px;line-height:25px;font-weight:300;font-style:normal;font-family:Poppins;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.dentrarario-home1-static-text2,.dentrarario-home1-static-text2{color:rgba(110,120,124,1.00);font-size:16px;line-height:25px;font-weight:300;font-style:normal;font-family:Poppins;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    }

    setREVStartSize({
        c: jQuery('#rev_slider_1_1'),
        gridwidth: [1240],
        gridheight: [517],
        sliderLayout: 'fullwidth'
    });

    var revapi1,
        tpj = jQuery;

    tpj(document).ready(function() {
		"use strict";
        if (tpj("#rev_slider_1_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1_1");
        } else {
            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "//dentario.themerex.net/wp-content/plugins/revslider/public/assets/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 7000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        touchOnDesktop: "off",
                        swipe_threshold: 75,
                        swipe_min_touches: 50,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 900,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        style: "",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 7,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
                    }
                },
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: 1240,
                gridheight: 517,
                lazyType: "none",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }

    });

    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = ".tp-caption.dentrarario-home3-static-text,.dentrarario-home3-static-text{color:rgba(255,255,255,1.00);font-size:16px;line-height:25px;font-weight:300;font-style:normal;font-family:Poppins;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.dentrarario-home3-static-header,.dentrarario-home3-static-header{color:rgba(255,255,255,1.00);font-size:42px;line-height:48px;font-weight:300;font-style:normal;font-family:Open Sans;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.dentrarario-home3-appointment-btn,.dentrarario-home3-appointment-btn{color:rgba(255,255,255,1.00);font-size:13px;line-height:16px;font-weight:400;font-style:normal;font-family:Poppins;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    }


	setREVStartSize({c: jQuery('#rev_slider_2_1'), gridwidth: [1240], gridheight: [517], sliderLayout: 'fullwidth'});

	var revapi2,
		tpj=jQuery;

	tpj(document).ready(function() {
			"use strict";
		if(tpj("#rev_slider_2_1").revolution == undefined){
			revslider_showDoubleJqueryError("#rev_slider_2_1");
		}else{
			revapi2 = tpj("#rev_slider_2_1").show().revolution({
				sliderType:"standard",
				jsFileLocation:"//dentario.themerex.net/wp-content/plugins/revslider/public/assets/js/",
				sliderLayout:"fullwidth",
				dottedOverlay:"none",
				delay:7000,
				navigation: {
					keyboardNavigation:"off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation:"off",
								mouseScrollReverse:"default",
					onHoverStop:"on",
					touch:{
						touchenabled:"on",
						touchOnDesktop:"off",
						swipe_threshold: 75,
						swipe_min_touches: 50,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					}
					,
					arrows: {
						style:"",
						enable:true,
						hide_onmobile:true,
						hide_under:900,
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1200,
						tmp:'',
						left: {
							h_align:"left",
							v_align:"center",
							h_offset:30,
							v_offset:0
						},
						right: {
							h_align:"right",
							v_align:"center",
							h_offset:30,
							v_offset:0
						}
					}
					,
					bullets: {
						enable:true,
						hide_onmobile:true,
						hide_under:600,
						style:"",
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1200,
						direction:"horizontal",
						h_align:"center",
						v_align:"bottom",
						h_offset:0,
						v_offset:30,
						space:7,
						tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
					}
				},
				visibilityLevels:[1240,1024,778,480],
				gridwidth:1240,
				gridheight:517,
				lazyType:"none",
				shadow:0,
				spinner:"off",
				stopLoop:"off",
				stopAfterLoops:-1,
				stopAtSlide:-1,
				shuffle:"off",
				autoHeight:"off",
				hideThumbsOnMobile:"off",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:770,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
			});
		}

	});	/*ready*/

	function revslider_showDoubleJqueryError(sliderID) {
		"use strict";
		var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
		errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
		errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
		errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
		errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
		jQuery(sliderID).show().html(errorMessage);
	}

}






function initFilters() {
	"use strict";
	jQuery(document).ready(function() {
		DENTARIO_STORAGE['ppp'] = 12;
		jQuery(".isotope_filters").append('<div class="isotope_filters_inner"><a href="#" data-filter="*" class="isotope_filters_button active">All</a><a href="#" data-filter=".flt_30" class="isotope_filters_button">Dental</a><a href="#" data-filter=".flt_29" class="isotope_filters_button">Dentistry</a><a href="#" data-filter=".flt_21" class="isotope_filters_button">Doctor</a><a href="#" data-filter=".flt_25" class="isotope_filters_button">Filling</a><a href="#" data-filter=".flt_22" class="isotope_filters_button">Dental Center</a><a href="#" data-filter=".flt_28" class="isotope_filters_button">Medical</a><a href="#" data-filter=".flt_20" class="isotope_filters_button">Stomatology</a></div>');
	});
};/*
 * jQuery Superfish Menu Plugin - v1.7.4
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

;(function ($) {
	"use strict";

	var methods = (function () {
		// private properties and methods go here
		var c = {
				bcClass: 'sf-breadcrumb',
				menuClass: 'sf-js-enabled',
				anchorClass: 'sf-with-ul',
				menuArrowClass: 'sf-arrows'
			},
			ios = (function () {
				var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
				if (ios) {
					// iOS clicks only bubble as far as body children
					$(window).load(function () {
						$('body').children().on('click', $.noop);
					});
				}
				return ios;
			})(),
			wp7 = (function () {
				var style = document.documentElement.style;
				return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
			})(),
			toggleMenuClasses = function ($menu, o) {
				var classes = c.menuClass;
				if (o.cssArrows) {
					classes += ' ' + c.menuArrowClass;
				}
				$menu.toggleClass(classes);
			},
			setPathToCurrent = function ($menu, o) {
				return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
					.addClass(o.hoverClass + ' ' + c.bcClass)
						.filter(function () {
							return ($(this).children(o.popUpSelector).hide().show().length);
						}).removeClass(o.pathClass);
			},
			toggleAnchorClass = function ($li) {
				$li.children('a').toggleClass(c.anchorClass);
			},
			toggleTouchAction = function ($menu) {
				var touchAction = $menu.css('ms-touch-action');
				touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
				$menu.css('ms-touch-action', touchAction);
			},
			applyHandlers = function ($menu, o) {
				var targets = 'li:has(' + o.popUpSelector + ')';
				if ($.fn.hoverIntent && !o.disableHI) {
					$menu.hoverIntent(over, out, targets);
				}
				else {
					$menu
						.on('mouseenter.superfish', targets, over)
						.on('mouseleave.superfish', targets, out);
				}
				var touchevent = 'MSPointerDown.superfish';
				if (!ios) {
					touchevent += ' touchend.superfish';
				}
				if (wp7) {
					touchevent += ' mousedown.superfish';
				}
				$menu
					.on('focusin.superfish', 'li', over)
					.on('focusout.superfish', 'li', out)
					.on(touchevent, 'a', o, touchHandler);
			},
			touchHandler = function (e) {
				var $this = $(this),
					$ul = $this.siblings(e.data.popUpSelector);

				if ($ul.length > 0 && $ul.is(':hidden')) {
					$this.one('click.superfish', false);
					if (e.type === 'MSPointerDown') {
						$this.trigger('focus');
					} else {
						$.proxy(over, $this.parent('li'))();
					}
				}
			},
			over = function () {
				var $this = $(this),
					o = getOptions($this);
				clearTimeout(o.sfTimer);
				$this.siblings().superfish('hide').end().superfish('show');
			},
			out = function () {
				var $this = $(this),
					o = getOptions($this);
				if (ios) {
					$.proxy(close, $this, o)();
				}
				else {
					clearTimeout(o.sfTimer);
					o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
				}
			},
			close = function (o) {
				o.retainPath = ($.inArray(this[0], o.$path) > -1);
				this.superfish('hide');

				if (!this.parents('.' + o.hoverClass).length) {
					o.onIdle.call(getMenu(this));
					if (o.$path.length) {
						$.proxy(over, o.$path)();
					}
				}
			},
			getMenu = function ($el) {
				return $el.closest('.' + c.menuClass);
			},
			getOptions = function ($el) {
				return getMenu($el).data('sf-options');
			};

		return {
			// public methods
			hide: function (instant) {
				if (this.length) {
					var $this = this,
						o = getOptions($this);
					if (!o) {
						return this;
					}
					var not = (o.retainPath === true) ? o.$path : '',
						$ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
						speed = o.speedOut;

					if (instant) {
						$ul.show();
						speed = 0;
					}
					o.retainPath = false;
					o.onBeforeHide.call($ul);
					$ul.stop(true, true).animate(o.animationOut, speed, function () {
						var $this = $(this);
						o.onHide.call($this);
					});
				}
				return this;
			},
			show: function () {
				var o = getOptions(this);
				if (!o) {
					return this;
				}
				var $this = this.addClass(o.hoverClass),
					$ul = $this.children(o.popUpSelector);

				o.onBeforeShow.call($ul);
				$ul.stop(true, true).animate(o.animation, o.speed, function () {
					o.onShow.call($ul);
				});
				return this;
			},
			destroy: function () {
				return this.each(function () {
					var $this = $(this),
						o = $this.data('sf-options'),
						$hasPopUp;
					if (!o) {
						return false;
					}
					$hasPopUp = $this.find(o.popUpSelector).parent('li');
					clearTimeout(o.sfTimer);
					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					// remove event handlers
					$this.off('.superfish').off('.hoverIntent');
					// clear animation's inline display style
					$hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
						return style.replace(/display[^;]+;?/g, '');
					});
					// reset 'current' path classes
					o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
					$this.find('.' + o.hoverClass).removeClass(o.hoverClass);
					o.onDestroy.call($this);
					$this.removeData('sf-options');
				});
			},
			init: function (op) {
				return this.each(function () {
					var $this = $(this);
					if ($this.data('sf-options')) {
						return false;
					}
					var o = $.extend({}, $.fn.superfish.defaults, op),
						$hasPopUp = $this.find(o.popUpSelector).parent('li');
					o.$path = setPathToCurrent($this, o);

					$this.data('sf-options', o);

					toggleMenuClasses($this, o);
					toggleAnchorClass($hasPopUp);
					toggleTouchAction($this);
					applyHandlers($this, o);

					$hasPopUp.not('.' + c.bcClass).superfish('hide', true);

					o.onInit.call(this);
				});
			}
		};
	})();

	$.fn.superfish = function (method, args) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		}
		else {
			return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
		}
	};

	$.fn.superfish.defaults = {
		popUpSelector: 'ul,.sf-mega', // within menu context
		hoverClass: 'sfHover',
		pathClass: 'overrideThisToUse',
		pathLevels: 1,
		delay: 800,
		animation: {opacity: 'show'},
		animationOut: {opacity: 'hide'},
		speed: 'normal',
		speedOut: 'fast',
		cssArrows: true,
		disableHI: false,
		onInit: $.noop,
		onBeforeShow: $.noop,
		onShow: $.noop,
		onBeforeHide: $.noop,
		onHide: $.noop,
		onIdle: $.noop,
		onDestroy: $.noop
	};

	// soon to be deprecated
	$.fn.extend({
		hideSuperfishUl: methods.hide,
		showSuperfishUl: methods.show
	});

})(jQuery);

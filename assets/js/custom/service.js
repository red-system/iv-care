jQuery(document).ready(function() {
    var body = document.body,
        html = document.documentElement;

    var height = Math.max( body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight );
    jQuery('#wrapper-loading').attr('style','height : '+height+'px')
    jQuery('.form-send').submit(function (e) {
        e.preventDefault();
        var form = jQuery(this)
        var formData = new FormData(form[0]);
        var action = form.attr('action');
        jQuery('#wrapper-loading').removeClass('hidden')
        jQuery.ajax({
            url: action,
            type: 'post',
            data: formData,
            async: false,
            beforeSend: function (xhr) {

                jQuery('.error-message').remove()
                jQuery('#contact_form_popup').addClass('mfp-hide')
            },
            error: function (request, error) {

                jQuery('#contact_form_popup').removeClass('mfp-hide')
            },
            success: function (data) {

                var response = jQuery.parseJSON(data)
                if(response.status=='success'){
                    swal.fire({
                        type: 'success',
                        title: 'Your message has send',
                    }).then(function() {
                        window.location.reload()
                    });
                } else {
                    jQuery('#contact_form_popup').removeClass('mfp-hide')
                    jQuery.each(response.errors, function (key, val) {
                        jQuery('[name="' + key + '"]').closest('div').append(val);
                    });
                }
            },
            complete: function (data) {
                jQuery('#wrapper-loading').addClass('hidden')
            }
            ,
            cache: false,
            contentType: false,
            processData: false
        });
    })

});
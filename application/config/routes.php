<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//$connection = mysqli_connect('159.65.132.116', 'danta', 'D4tnka!#$f13Fda', 'iv_care_bali');
$connection = mysqli_connect('localhost', 'danta', 'anY73SvgB3IPkHIy', 'iv_care_bali');

$route['default_controller'] = 'home';


$route['proweb'] = 'proweb/login';
$route['proweb/login'] = 'proweb/login/process';
$route['layanan'] = 'layanan';
$route['layanan/(:num)'] = 'layanan';
$route['sub-layanan'] = 'layanan';
$route['sub-layanan/(:num)'] = 'layanan';
$route['testimony'] = 'testimony';
$route['gallery'] = 'gallery';
$route['gallery/more'] = 'gallery/more';
$route['blog-news'] = 'blog_news';
$route['blog-news/page/:num'] = 'blog_news';

$route['artikel/(:num)'] = 'artikel';
$route['about-us'] = 'about_us';
$route['doctor'] = 'doctor';
$route['nurse'] = 'nurse';
$route['contact'] = 'contact';
$route['contact/send-message'] = 'contact/send_message';
$route['book-now'] = 'booking';
$route['kontak/kirim-pesan'] = 'kontak/kirim_pesan';
$route['syarat-ketentuan'] = 'syarat_ketentuan';
$route['kebijakan-privasi'] = 'kebijakan_privasi';
$route['faq'] = 'FAQ';


$blog_category = mysqli_query($connection, "SELECT id, title FROM blog");
while ($data = mysqli_fetch_array($blog_category)) {
    $route['blog-news/' . slug($data['title'])] = "blog_news/detail/".$data['id'];
}

$services = mysqli_query($connection, "SELECT id, title FROM layanan");
while ($data = mysqli_fetch_array($services)) {
    $route['service/' . slug($data['title'])] = "service/category/".$data['id'];
}

$service_sub = mysqli_query($connection, "SELECT id, title FROM sub_layanan");
while ($data = mysqli_fetch_array($service_sub)) {
    $route['sub-layanan/' . slug($data['title'])] = "layanan/detail/".$data['id'];
}

$route['404_override'] = 'PageNotFound';

$route['translate_uri_dashes'] = FALSE;

function slug($string)
{
    $find = array(' ','’',':', '/', '&', '\\', '\'', ',','(',')');
    $replace = array('-','-','-', '-', 'and', '-', '-', '-','','');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
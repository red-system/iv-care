<?php

class M_faq extends CI_Model
{

	protected $table = 'layanan';

	public function get_data()
	{
	    $this->db->where('category','faq');
		return $this->db->get($this->table);
	}
    public function get_data_home()
    {
        $this->db->where('category','faq');
        $this->db->where('display_at_home','yes');
        return $this->db->get($this->table);
    }
	public function row_data($where)
	{
		return $this->db->where($where)->get($this->table)->row();
	}

	public function input_data($data)
	{
		$this->db->insert($this->table, $data);
	}

	public function delete_data($where)
	{
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	function update_data($where, $data)
	{
		$this->db->where($where);
		$this->db->update($this->table, $data);
	}
}

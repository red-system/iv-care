<section class="slider_wrap slider_fullwide slider_engine_revo">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery">
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" data-version="5.4.3">
            <ul>
                <?php foreach ($sliders as $key => $item) { ?>
                    <li data-index="rs-<?=($key+1)?>" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?php echo base_url();?>assets/images/01_slider-background-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <img src="<?php echo base_url();?>assets/images/01_slider-background.jpg" alt="" title="01_slider-background" width="2340" height="1034" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <div class="tp-caption tp-resizeme" id="slide-1-layer-1" data-x="587" data-y="" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-type="image" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":750,"ease":"Power2.easeOut"},{"delay":4000,"speed":1500,"to":"sX:1.25;sY:1.25;opacity:0;","ease":"Power0.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                            <img src="<?php echo base_url().'upload/images/'.$item->thumbnail;?>" alt="" data-ww="623px" data-hh="517px" width="623" height="517" data-no-retina>
                        </div>
                        <div class="tp-caption dentrarario-home1-static-text2 tp-resizeme" id="slide-1-layer-2" data-x="35" data-y="255" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power2.easeOut"},{"delay":4750,"speed":1000,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                            Dr. Joseph Phillips is an active member of the Academy of
                            <br/> General Dentistry and strives to stay up to date with the latest
                            <br/> in techniques and technology in the dental profession.
                        </div>
                        <div class="tp-caption dentrarario-home1-static-text tp-resizeme button" id="slide-1-layer-3" data-x="35" data-y="340" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},{"delay":3750,"speed":1000,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                            <a href="<?=site_url().'contact'?>" class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium alignleft">Make an appointment</a>
                        </div>
                        <div class="tp-caption dentrarario-home1-static-header tp-resizeme" id="slide-1-layer-4" data-x="35" data-y="143" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":4750,"speed":1000,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                            High Innovative Technology
                            <br/> & Professional Dentists
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">

                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <h2 class="sc_title sc_title_regular sc_align_center margin_top_huge margin_bottom_null centext">Welcome to <?=$web_name?></h2>
                                    <h6 class="vc_custom_heading vc_custom_1455547755372">Highest level of service you can find</h6>
                                    <div class="columns_wrap sc_columns columns_nofluid autoheight sc_columns_count_2">
                                        <div class="column-1_2 sc_column_item sc_column_item_1">
                                            <div class="sc_column_item_inner bgimage_column" style="background-image: url(<?=base_url().'upload/images/'.$page_image?>)"></div>
                                        </div>
                                        <div class="column-1_2 sc_column_item sc_column_item_2">
                                            <div class="vc_empty_space hidden-xs space_50p">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <?=$about_page?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1455704765009">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_services_047_wrap" class="sc_services_wrap">
                                        <div id="sc_services_047" class="sc_services sc_services_style_services-1 sc_services_type_icons  margin_top_huge margin_bottom_large">
                                            <h2 class="sc_services_title sc_item_title">Our Clinic Services</h2>
                                            <div class="sc_services_descr sc_item_descr">Services we provide</div>
                                            <div class="sc_columns columns_wrap">
                                                <?php foreach ($layanan as $item) { ?>
                                                <div class="column-1_3 column_padding_bottom">
                                                    <div id="sc_services_047_1" class="sc_services_item sc_services_item_1">
                                                        <a href="<?=$item->menu_url?>">
                                                            <img src="<?=base_url().'upload/images/'.$item->thumbnail?>" width="80px" class="">
                                                        </a>
                                                        <div class="sc_services_item_content">
                                                            <h4 class="sc_services_item_title">
                                                                <a href="<?=$item->menu_url?>"><?=$item->title?></a>
                                                            </h4>
                                                            <div class="sc_services_item_description">
                                                                <p><?=$item->title_sub?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="columns_wrap sc_columns columns_fluid autoheight no_margins sc_columns_count_3">

                                        <div class="column-1_3 sc_column_item sc_column_item_1">
                                            <div class="sc_column_item_inner bgc_1">
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                                <div id="sc_services_269_wrap" class="sc_services_wrap sc_services_inverse">
                                                    <div id="sc_services_269" class="sc_services sc_services_style_services-5 sc_services_type_icons">
                                                        <div id="sc_services_269_1" class="sc_services_item sc_services_item_1">
                                                            <a href="#">
                                                                <img src="<?=base_url().'assets/images/experience.png'?>" class="sc_icon">
                                                            </a>
                                                            <div class="sc_services_item_content">
                                                                <h4 class="sc_services_item_title">
                                                                    <a href=""><?=$count[0]->title?></a>
                                                                </h4>
                                                                <div class="sc_services_item_description">
                                                                    <p><?=$count[0]->sub_title?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_3 sc_column_item sc_column_item_2">
                                            <div class="sc_column_item_inner bgc_2">
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                                <div id="sc_services_260_wrap" class="sc_services_wrap sc_services_inverse">
                                                    <div id="sc_services_260" class="sc_services sc_services_style_services-5 sc_services_type_icons">
                                                        <div id="sc_services_260_1" class="sc_services_item sc_services_item_1">
                                                            <a href="#">
                                                                <a href="#">
                                                                    <img src="<?=base_url().'assets/images/heppy-customer.png'?>" class="sc_icon">
                                                                </a>
                                                            </a>
                                                            <div class="sc_services_item_content">
                                                                <h4 class="sc_services_item_title">
                                                                    <a href="#"><?=$count[1]->title?></a>
                                                                </h4>
                                                                <div class="sc_services_item_description">
                                                                    <p><?=$count[1]->sub_title?></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_3 sc_column_item sc_column_item_3">
                                            <div class="sc_column_item_inner bgc_3">
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                                <div id="sc_services_501_wrap" class="sc_services_wrap sc_services_inverse">
                                                    <div id="sc_services_501" class="sc_services sc_services_style_services-5 sc_services_type_icons">
                                                        <div id="sc_services_501_1" class="sc_services_item sc_services_item_1">
                                                            <a href="#">
                                                                <img src="<?=base_url().'assets/images/medical-service.png'?>" class="sc_icon">
                                                            </a>
                                                            <div class="sc_services_item_content">
                                                                <h4 class="sc_services_item_title">
                                                                    <a href="#"><?=$count[2]->title?></a>
                                                                </h4>
                                                                <div class="sc_services_item_description">
                                                                    <p><?=$count[1]->sub_title?></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space hidden-xs space_32p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_testimonials_359" class="sc_testimonials sc_testimonials_style_testimonials-1 margin_top_huge margin_bottom_huge">
                                        <h2 class="sc_testimonials_title sc_item_title">Our Happy Clients</h2>
                                        <div class="sc_testimonials_descr sc_item_descr">What people say about us</div>
                                        <div class="sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_controls_side" data-interval="6521" data-slides-min-width="250">
                                            <div class="slides swiper-wrapper">
                                                <?php foreach ($testimony as $item) {
                                                    ?>
                                                    <div class="swiper-slide" data-style="width:100%;">
                                                        <div id="sc_testimonials_359_1" class="sc_testimonial_item">
                                                            <div class="sc_testimonial_avatar">
                                                                <img width="79" height="79" alt="" src="<?php echo base_url().'upload/images/'.$item->thumbnail;?>">
                                                            </div>
                                                            <div class="sc_testimonial_content">
                                                                <p>
                                                                    <?=$item->description?>
                                                                </p>

                                                            </div>
                                                            <div class="sc_testimonial_author">
                                                                <span class="sc_testimonial_author_name"><?=$item->title?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } ?>
                                            </div>
                                            <div class="sc_slider_controls_wrap">
                                                <a class="sc_slider_prev" href="#"></a>
                                                <a class="sc_slider_next" href="#"></a>
                                            </div>
                                            <div class="sc_slider_pagination_wrap"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1455705010924">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_team_454_wrap" class="sc_team_wrap type1">
                                        <div id="sc_team_454" class="sc_team sc_team_style_team-1  margin_top_huge margin_bottom_large">
                                            <h2 class="sc_team_title sc_item_title">Meet Our Team</h2>
                                            <div class="sc_team_descr sc_item_descr">Best specialists in one place</div>
                                            <div class="sc_columns columns_wrap">
                                                <?php foreach ($team as $item) {
                                                    ?>
                                                    <div class="column-1_4 column_padding_bottom">
                                                        <div id="sc_team_454_1" class="sc_team_item">
                                                            <div class="sc_team_item_avatar">
                                                                <img width="182" height="182" alt="" src="<?php echo base_url().'upload/images/'.$item->thumbnail;?>">
                                                            </div>
                                                            <div class="sc_team_item_info">
                                                                <h5 class="sc_team_item_title"><a href="<?=site_url().'doctor'?>"><?=$item->title?></a></h5>
                                                                <div class="sc_team_item_position"><?=$item->position?></div>
                                                                <div class="sc_team_item_description">
                                                                   <?=$item->description?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <h2 class="sc_team_title sc_item_title">LATEST NEWS</h2>
                                <div class="sc_team_descr sc_item_descr">We will always keep you informed about fresh news of our IV Care Bali.</div>
                                <div class="wpb_wrapper">
                                    <div class="sc_columns columns_wrap" style="margin-top: 30px">
                                        <?php
                                        foreach ($blog as $item){
                                            ?>
                                            <div class="column-1_3 column_padding_bottom">
                                                <article class="post_item post_item_masonry post_item_masonry_3">
                                                    <div class="post_featured">
                                                        <div class="post_thumb" data-image="<?=base_url().'upload/images/'.$item->thumbnail?>" data-title="<?=$item->title?>">
                                                            <a class="hover_icon hover_icon_link" href="<?=$item->menu_url?>">
                                                                <img width="370" height="246" alt="" src="<?=base_url().'upload/images/'.$item->thumbnail?>">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post_content isotope_item_content">
                                                        <h6 class="post_title">
                                                            <a href="<?=$item->menu_url?>"><?=$item->title?></a>
                                                        </h6>
                                                        <div class="post_info">
                                            <span class="post_info_item post_info_posted">
                                                <a href="<?=$item->menu_url?>" class="post_info_date"><?=date('F d ,Y',strtotime($item->created_at))?></a>
                                            </span>
                                                            <span class="post_info_item post_info_posted_by">by
                                                <a href="<?=$item->menu_url?>" class="post_info_author"><?=$item->author?></a>
                                            </span>
                                                            <span class="post_info_item post_info_tags">in
                                                <a class="category_link" href="<?=$item->menu_url?>"><?=$item->category?></a>,
                                            </span>
                                                        </div>
                                                        <div class="post_descr">
                                                            <p><?=$item->short_description?></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php if(sizeof($partner>0)){?>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1455705010924">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_team_454_wrap" class="sc_team_wrap type1">
                                        <div id="sc_team_454" class="sc_team sc_team_style_team-1  margin_top_huge margin_bottom_large">
                                            <h2 class="sc_team_title sc_item_title">Our Partner</h2>
                                            <div class="sc_team_descr sc_item_descr">Best specialists in one place</div>
                                            <div class="sc_columns columns_wrap">
                                                <?php foreach ($partner as $item) { ?>
                                                <div class="column-1_4 column_padding_bottom">
                                                    <div id="sc_team_454_1" class="sc_team_item">
                                                        <div class="sc_team_item_avatar">
                                                            <img width="182" height="182" alt="" src="<?php echo base_url().'upload/images/'.$item->thumbnail?>">
                                                        </div>
                                                        <div class="sc_team_item_info">
                                                            <h5 class="sc_team_item_title"><?=$item->title?></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <?php } ?>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="columns_wrap sc_columns columns_nofluid no_margins sc_columns_count_2">
                                        <div class="column-1_2 sc_column_item sc_column_item_1">
                                            <div class="sc_section w_half_content alignright">
                                                <div class="sc_section_inner">
                                                    <h2 class="sc_title sc_title_regular sc_align_left margin_top_large margin_bottom_tiny">Contact Us</h2>
                                                    <h6 class="vc_custom_heading vc_custom_1455550892710">Don not hesitate to contact Us!</h6>
                                                    <div id="sc_services_086_wrap" class="sc_services_wrap">
                                                        <div id="sc_services_086" class="sc_services sc_services_style_services-2 sc_services_type_icons  margin_top_medium alignleft">
                                                            <div id="sc_services_086_1" class="sc_services_item sc_services_item_1">
                                                                <span class="sc_icon icon-location-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Our Adress</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p><?=$alamat?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="sc_services_086_2" class="sc_services_item sc_services_item_2">
                                                                <span class="sc_icon icon-mobile-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Phone</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p><a href="<?=$telephone_link?>"><?=$telephone?></a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="sc_services_086_3" class="sc_services_item sc_services_item_3">
                                                                <span class="sc_icon icon-calendar-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Open Hours</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p><?=$jam_kerja?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_2 sc_column_item sc_column_item_2">
                                            <div class="sc_section w_half_content">
                                                <div class="sc_section_inner">
                                                    <h2 class="sc_title sc_title_regular sc_align_left margin_top_large margin_bottom_tiny">FAQ</h2>
                                                    <div class="sc_accordion" data-active="0">
                                                        <?php foreach ($faq as $item){ ?>
                                                            <div class="sc_accordion_item">
                                                                <h5 class="sc_accordion_title">
                                                                    <span class="sc_accordion_icon sc_accordion_icon_closed icon-plus"></span>
                                                                    <span class="sc_accordion_icon sc_accordion_icon_opened icon-minus"></span>
                                                                   <?=$item->title?>
                                                                </h5>
                                                                <div class="sc_accordion_content">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <?=$item->description?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>


<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="sc_columns columns_wrap" style="margin-top: 30px">
                        <?php
                        foreach ($blogs as $item){
                            ?>
                            <div class="column-1_3 column_padding_bottom">
                                <article class="post_item post_item_masonry post_item_masonry_3">
                                    <div class="post_featured">
                                        <div class="post_thumb" data-image="<?=base_url().'upload/images/'.$item->thumbnail?>" data-title="<?=$item->title?>">
                                            <a class="hover_icon hover_icon_link" href="<?=$item->menu_url?>">
                                                <img width="370" height="246" alt="" src="<?=base_url().'upload/images/'.$item->thumbnail?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post_content isotope_item_content">
                                        <h6 class="post_title">
                                            <a href="<?=$item->menu_url?>"><?=$item->title?></a>
                                        </h6>
                                        <div class="post_info">
                                            <span class="post_info_item post_info_posted">
                                                <a href="<?=$item->menu_url?>" class="post_info_date"><?=date('F d ,Y',strtotime($item->created_at))?></a>
                                            </span>
                                            <span class="post_info_item post_info_posted_by">by
                                                <a href="<?=$item->menu_url?>" class="post_info_author"><?=$item->author?></a>
                                            </span>
                                            <span class="post_info_item post_info_tags">in
                                                <a class="category_link" href="<?=$item->menu_url?>"><?=$item->category?></a>,
                                            </span>
                                        </div>
                                        <div class="post_descr">
                                            <p><?=$item->short_description?></p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <nav id="pagination" class="pagination_wrap pagination_pages">
                        <?=$pagination?>
                    </nav>

                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
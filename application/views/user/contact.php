<div class="top_panel_title top_panel_style_2 title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_2 title_present_inner breadcrumbs_present_inner">
        <div class="content_wrap">
            <h1 class="page_title">Contact Us</h1>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="<?=site_url()?>">Home</a>
                <span class="breadcrumbs_delimiter"></span>
                <span class="breadcrumbs_item current">Contact Us</span>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <h2 class="sc_title sc_title_regular sc_align_center margin_top_huge margin_bottom_tiny centext">How To Find Us</h2>
                                    <h3 class="vc_custom_heading">Address and Direction</h3>
                                    <div class="columns_wrap sc_columns columns_nofluid autoheight sc_columns_count_2">
                                        <div class="column-1_2 sc_column_item centext">
                                            <div class="sc_column_item_inner bgimage_column" style="background-image: url(<?=base_url().'upload/images/'.$page_image?>)"></div>
                                        </div>
                                        <div class="column-1_2 sc_column_item">
                                            <div id="sc_services_604_wrap" class="sc_services_wrap">
                                                <div id="sc_services_604" class="sc_services sc_services_style_services-2 sc_services_type_icons margin_top_medium alignleft">
                                                    <div id="sc_services_604_1" class="sc_services_item">
                                                        <span class="sc_icon icon-location-light"></span>
                                                        <div class="sc_services_item_content">
                                                            <h4 class="sc_services_item_title">Our Adress</h4>
                                                            <div class="sc_services_item_description">
                                                                <p><?=$alamat?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="sc_services_604_2" class="sc_services_item">
                                                        <span class="sc_icon icon-mobile-light"></span>
                                                        <div class="sc_services_item_content">
                                                            <h4 class="sc_services_item_title">Phone</h4>
                                                            <div class="sc_services_item_description">
                                                                <p><a href="<?=$telephone_link?>"><?=$telephone?></a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="sc_services_604_3" class="sc_services_item">
                                                        <span class="sc_icon icon-calendar-light"></span>
                                                        <div class="sc_services_item_content">
                                                            <h4 class="sc_services_item_title">Open Hours</h4>
                                                            <div class="sc_services_item_description">
                                                                <p><?=$jam_kerja?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="contact_form_popup" class="sc_popup mfp-with-anim mfp-hide">
                                                <div id="sc_form_020_wrap" class="sc_form_wrap">
                                                    <div id="sc_form_329_wrap" class="sc_form_wrap">
                                                        <div id="sc_form_329" class="sc_form sc_form_style_form_2 margin_top_huge margin_bottom_huge">
                                                            <h2 class="sc_form_title sc_item_title">Contact Form</h2>
                                                            <div class="sc_form_descr sc_item_descr">You can contact us anytime</div>
                                                            <form id="sc_form_329_form" data-formtype="form_2" method="post" action="<?=site_url().'contact/send-message'?>" class="inited form-send">
                                                                <div class="sc_form_info">
                                                                    <div class="sc_form_item label_over">
                                                                        <div class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="sc_form_item sc_form_field label_over">
                                                                                    <i class="icon  icon-user-light"></i>
                                                                                    <label class="required" for="sc_form_username">Name</label>
                                                                                    <input id="sc_form_username" type="text" autocomplete="off" name="nama" placeholder="Name *">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="sc_form_item label_over">
                                                                        <div class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="sc_form_item sc_form_field label_over">
                                                                                    <i class="icon icon-mobile-light"></i>
                                                                                    <label class="required" for="sc_form_phone">Phone</label>
                                                                                    <input id="sc_form_phone" type="text" autocomplete="off" name="phone" placeholder="Phone (Ex. +1-234-567-890)">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="sc_form_item label_over">
                                                                        <div class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="sc_form_item sc_form_field label_over">
                                                                                    <i class="icon icon-mail-light"></i>
                                                                                    <label class="required" for="sc_form_email">E-mail</label>
                                                                                    <input id="sc_form_email" type="text" autocomplete="off" name="email" placeholder="E-mail *">
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="sc_form_item sc_form_message label_over">
                                                                    <label class="required" for="sc_form_message">Message</label>
                                                                    <textarea id="sc_form_message" rows="1" name="message" placeholder="Message"></textarea>
                                                                </div>
                                                                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2">
                                                                    <div class="column-1_2 sc_column_item">
                                                                        <div class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="sc_form_item sc_form_field label_over">
                                                                                    <i class="icon icon-mail-light"></i>
                                                                                    <label class="required" for="sc_form_email">E-mail</label>
                                                                                    <div class="sc_form_item sc_form_field">
                                                                                        <?php echo $captcha; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="column-1_2 sc_column_item">
                                                                        <div class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="sc_form_item sc_form_field label_over">
                                                                                    <i class="icon"></i>
                                                                                    <label class="required" for="sc_form_doctor">Capcha</label>
                                                                                    <input id="" type="text" name="captcha" placeholder="Captcha">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="sc_form_item sc_form_button">
                                                                    <button class="aligncenter">Send</button>
                                                                </div>
                                                                <div class="result sc_infobox"></div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#contact_form_popup" class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium aligncenter margin_bottom_medium sc_popup_link">Use contact form</a>
                                            <div class="vc_empty_space space_20p">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
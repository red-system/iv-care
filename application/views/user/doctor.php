<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_empty_space space_93p">
                                        <span class="vc_empty_space_inner"></span>
                                    </div>
                                    <div id="sc_team_910_wrap" class="sc_team_wrap type2">
                                        <div id="sc_team_910" class="sc_team sc_team_style_team-2 ">
                                            <h2 class="sc_team_title sc_item_title">Best specialists in one place</h2>
                                            <div class="sc_team_descr sc_item_descr">Your comfort is our priority </div>
                                            <div class="sc_columns columns_wrap">
                                                <?php
                                                foreach ($page as $key){
                                                    ?>
                                                    <div class="column-1_4 column_padding_bottom">
                                                        <div id="sc_team_910_1" class="sc_team_item">
                                                            <div class="sc_team_item_avatar">
                                                                <img width="240" height="240" alt="" src="<?=base_url().'upload/images/'.$key->thumbnail?>">
                                                            </div>
                                                            <div class="sc_team_item_info">
                                                                <h5 class="sc_team_item_title">
                                                                    <a href="<?=current_url()?>"> Eleonore Grey</a>
                                                                </h5>
                                                                <div class="sc_team_item_position"><?=$key->position?></div>
                                                                <div class="sc_team_item_description"><?=$key->description?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space space_70p">
                                        <span class="vc_empty_space_inner"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
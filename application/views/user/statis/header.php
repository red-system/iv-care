<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/images/favicon.png" />
    <title><?=$web_name?></title>

    <?php
        foreach ($css as $item){
            ?>
            <link rel='stylesheet' href='<?=base_url().'assets/'.$item?>' type='text/css' media='all' />
            <?php
        }
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body class="indexp home page body_style_wide body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide vc_responsive">
<input type="hidden" id="base_url" value="<?=site_url()?>">
<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="<?=site_url()?>" data-separator="yes"></a>
<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
<div id="wrapper-loading" class="wrapper-loading hidden">
    <div class="wrapper-loading-2">
        <div class="load">
            <img src="<?=site_url().'assets/images/spiner.gif'?>">
        </div>
    </div>
</div>
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        <header class="top_panel_wrap top_panel_style_2 scheme_original">
            <div class="top_panel_wrap_inner top_panel_inner_style_2 top_panel_position_above">
                <div class="top_panel_middle">
                    <div class="content_wrap">
                        <div class="columns_wrap columns_fluid">
                            <div class="column-1_3 contact_field contact_phone_wrap">
                                <span class="contact_icon icon-phone"></span>
                                <span class="contact_label contact_phone">Free Call <strong><a href="<?=$telephone_link?>"><?=$telephone?></a></strong></span>
                                <span class="contact_address"><?=$alamat?></span>
                            </div>
                            <div class="column-1_3 contact_logo">
                                <div class="logo">
                                    <a href="<?=site_url()?>">
                                        <img src="<?=base_url()?>assets/images/logo-1x.png" class="logo_main" alt="" style="width: 140px">
                                    </a>
                                </div>
                            </div>
                            <div class="column-1_3 contact_field open_hours_wrap">
                                <span class="contact_icon icon-calendar-light"></span>
                                <span class="open_hours_label">Open hours:</span>
                                <span class="open_hours_text"><?=$jam_kerja?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_panel_bottom">
                    <div class="content_wrap clearfix">
                        <nav class="menu_main_nav_area">
                            <ul id="menu_main" class="menu_main_nav">
                                <li class="menu-item"><a href="<?=base_url()?>"><span>Home</span></a></li>
                                <li class="menu-item menu-item-has-children"><a href="#"><span>Services</span></a>
                                    <ul class="sub-menu">

                                        <?php
                                        foreach ($menu_layanan as $key){
                                            ?>
                                            <li class="menu-item"><a href="<?=base_url().'service/'.$key->menu_url?>"><span><?=$key->title?></span></a></li>
                                            <?php
                                        }
                                        ?>

                                    </ul>
                                </li>
                                <li class="menu-item menu-item-has-children"><a href="#"><span>About</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="<?=base_url()?>about-us"><span>About Us</span></a></li>
                                        <li class="menu-item menu-item-has-children"><a href="#"><span>Our Team</span></a>
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a href="<?=base_url()?>doctor"><span>Doctor</span></a></li>
                                                <li class="menu-item"><a href="<?=base_url()?>nurse"><span>Nurse</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="<?=base_url()?>gallery"><span>Gallery</span></a>
                                </li>
                                <li class="menu-item"><a href="<?=base_url()?>blog-news"><span>News & Blog</span></a>
                                </li>
                                <li class="menu-item"><a href="<?=base_url()?>testimony"><span>Testimony</span></a></li>
                                <li class="menu-item"><a href="<?=base_url()?>contact"><span>Contacts</span></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div class="header_mobile">
            <div class="content_wrap">
                <div class="menu_button icon-menu"></div>
                <div class="logo">
                    <a href="<?=site_url()?>">
                        <img src="<?=base_url()?>assets/images/logo-1x.png" class="logo_main" alt="" width="202" height="49">
                    </a>
                </div>
            </div>
            <div class="side_wrap">
                <div class="close">Close</div>
                <div class="panel_top">
                    <nav class="menu_main_nav_area">
                        <ul id="menu_main_mobile" class="menu_main_nav">
                            <li class="menu-item"><a href="<?=base_url()?>"><span>Home</span></a></li>
                            <li class="menu-item menu-item-has-children"><a href="#"><span>Services</span></a>
                                <ul class="sub-menu">

                                    <?php
                                    foreach ($menu_layanan as $key){
                                        ?>
                                        <li class="menu-item"><a href="<?=base_url().'service/'.$key->menu_url?>"><span><?=$key->title?></span></a></li>
                                        <?php
                                    }
                                    ?>

                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children"><a href="#"><span>About</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="<?=base_url()?>about-us"><span>About Us</span></a></li>
                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Our Team</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="<?=base_url()?>doctor"><span>Doctor</span></a></li>
                                            <li class="menu-item"><a href="<?=base_url()?>nurse"><span>Nurse</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item"><a href="<?=base_url()?>gallery"><span>Gallery</span></a>
                            </li>
                            <li class="menu-item"><a href="<?=base_url()?>blog-news"><span>News & Blog</span></a>
                            </li>
                            <li class="menu-item"><a href="<?=base_url()?>testimony"><span>Testimony</span></a></li>
                            <li class="menu-item"><a href="<?=base_url()?>contact"><span>Contacts</span></a></li>
                        </ul>
                    </nav>

                </div>
            </div>
            <div class="mask"></div>
        </div>

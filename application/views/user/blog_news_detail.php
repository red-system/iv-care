<div class="top_panel_title top_panel_style_2 title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_2 title_present_inner breadcrumbs_present_inner">
        <div class="content_wrap">
            <h1 class="page_title"><?=$pages->title?></h1>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="<?=base_url()?>">Home</a>
                <span class="breadcrumbs_delimiter"></span>
                <a class="breadcrumbs_item cat_post" href="<?=base_url().'blog-news'?>">News & Blog</a>
                <span class="breadcrumbs_delimiter"></span>
                <span class="breadcrumbs_item current"><?=$pages->title?></span>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="page_content_wrap page_paddings_yes">
                        <div class="content_wrap">
                            <div class="content">
                                <article class="post_item post_item_excerpt post has-post-thumbnail hentry">
                                    <div class="post_info post_info_top">
                                        <span class="post_info_item post_info_posted">
                                            <a href="<?=current_url()?>" class="post_info_date date updated" content="<?=$pages->created_at?>"><?=date('F d ,Y',strtotime($item->created_at))?></a>
                                        </span>
                                                <span class="post_info_item post_info_posted_by vcard">by
                                            <a href="<?=current_url()?>" class="post_info_author"><?=$pages->author?></a>
                                        </span>
                                                <span class="post_info_item post_info_tags">in
                                            <a class="category_link" href="<?=current_url()?>"><?=$pages->category?></a>
                                        </span>

                                    </div>
                                    <div class="post_featured">

                                        <div class="post_thumb" data-image="<?=base_url().'upload/images/'.$pages->thumbnail?>" data-title="Image Post">
                                            <a class="hover_icon hover_icon_link" href="<?=current_url()?>">
                                                <img width="770" height="434" alt="Image Post" src="<?=base_url().'upload/images/'.$pages->thumbnail?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post_content clearfix">
                                        <div class="post_descr">
                                            <?=$pages->description?>

                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_testimonials_359" class="sc_testimonials sc_testimonials_style_testimonials-1 margin_top_huge margin_bottom_huge">
                                        <h2 class="sc_testimonials_title sc_item_title">Our Happy Clients</h2>
                                        <div class="sc_testimonials_descr sc_item_descr">What people say about us</div>
                                        <div class="sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_controls_side" data-interval="6521" data-slides-min-width="250">
                                            <div class="slides swiper-wrapper">
                                                <?php foreach ($testimony as $item) {
                                                    ?>
                                                    <div class="swiper-slide" data-style="width:100%;">
                                                        <div id="sc_testimonials_359_1" class="sc_testimonial_item">
                                                            <div class="sc_testimonial_avatar">
                                                                <img width="79" height="79" alt="" src="<?php echo base_url().'upload/images/'.$item->thumbnail;?>">
                                                            </div>
                                                            <div class="sc_testimonial_content">
                                                                <p>
                                                                    <?=$item->description?>
                                                                </p>

                                                            </div>
                                                            <div class="sc_testimonial_author">
                                                                <span class="sc_testimonial_author_name"><?=$item->title?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } ?>
                                            </div>
                                            <div class="sc_slider_controls_wrap">
                                                <a class="sc_slider_prev" href="#"></a>
                                                <a class="sc_slider_next" href="#"></a>
                                            </div>
                                            <div class="sc_slider_pagination_wrap"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>

        <div class="page_content_wrap page_paddings_no">
            <div class="content_wrap">
                <div class="content">
                    <article class="post_item post_item_single page hentry">
                        <section class="post_content">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h2 class="sc_title sc_title_regular sc_align_center margin_top_huge margin_bottom_null centext">Welcome to <?=$web_name?></h2>
                                            <h6 class="vc_custom_heading vc_custom_1455547755372">Highest level of service you can find</h6>
                                            <div class="columns_wrap sc_columns columns_nofluid autoheight sc_columns_count_2">
                                                <div class="column-1_2 sc_column_item sc_column_item_1">
                                                    <div class="sc_column_item_inner bgimage_column" style="background-image: url(<?=base_url().'upload/images/'.$page_image?>)"></div>
                                                </div>
                                                <div class="column-1_2 sc_column_item sc_column_item_2">
                                                    <div class="vc_empty_space hidden-xs space_50p">
                                                        <span class="vc_empty_space_inner"></span>
                                                    </div>

                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <?=$about_page?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1455704765009">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div id="sc_services_047_wrap" class="sc_services_wrap">
                                                <div id="sc_services_047" class="sc_services sc_services_style_services-1 sc_services_type_icons  margin_top_huge margin_bottom_large">
                                                    <h2 class="sc_services_title sc_item_title">Our Clinic Services</h2>
                                                    <div class="sc_services_descr sc_item_descr">Services we provide</div>
                                                    <div class="sc_columns columns_wrap">
                                                        <?php foreach ($layanan as $item) { ?>
                                                            <div class="column-1_3 column_padding_bottom">
                                                                <div id="sc_services_047_1" class="sc_services_item sc_services_item_1">
                                                                    <a href="<?=base_url().'service/'.$item->url_title?>">
                                                                        <img src="<?=base_url().'upload/images/'.$item->thumbnail?>" width="80px" class="">
                                                                    </a>
                                                                    <div class="sc_services_item_content">
                                                                        <h4 class="sc_services_item_title">
                                                                            <a href="<?=base_url().'service/'.$item->url_title?>"><?=$item->title?></a>
                                                                        </h4>
                                                                        <div class="sc_services_item_description">
                                                                            <p><?=$item->title_sub?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width"></div>

                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div id="sc_testimonials_359" class="sc_testimonials sc_testimonials_style_testimonials-1 margin_top_huge margin_bottom_huge">
                                                <h2 class="sc_testimonials_title sc_item_title">Our Happy Clients</h2>
                                                <div class="sc_testimonials_descr sc_item_descr">What people say about us</div>
                                                <div class="sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_controls_side" data-interval="6521" data-slides-min-width="250">
                                                    <div class="slides swiper-wrapper">
                                                        <?php foreach ($testimony as $item) {
                                                            ?>
                                                            <div class="swiper-slide" data-style="width:100%;">
                                                                <div id="sc_testimonials_359_1" class="sc_testimonial_item">
                                                                    <div class="sc_testimonial_avatar">
                                                                        <img width="79" height="79" alt="" src="<?php echo base_url().'upload/images/'.$item->thumbnail;?>">
                                                                    </div>
                                                                    <div class="sc_testimonial_content">
                                                                        <p>
                                                                            <?=$item->description?>
                                                                        </p>

                                                                    </div>
                                                                    <div class="sc_testimonial_author">
                                                                        <span class="sc_testimonial_author_name"><?=$item->title?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                    <div class="sc_slider_controls_wrap">
                                                        <a class="sc_slider_prev" href="#"></a>
                                                        <a class="sc_slider_next" href="#"></a>
                                                    </div>
                                                    <div class="sc_slider_pagination_wrap"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </article>
                    <section class="related_wrap related_wrap_empty"></section>
                </div>
            </div>
        </div>

<div class="top_panel_title top_panel_style_2 title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_2 title_present_inner breadcrumbs_present_inner">
        <div class="content_wrap">
            <h1 class="page_title">Book Now</h1>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="<?=base_url()?>">Home</a>
                <span class="breadcrumbs_delimiter"></span>
                <span class="breadcrumbs_item current">Book Now</span>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid scheme_light">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="sc_form_445_wrap" class="sc_form_wrap">
                                        <div id="sc_form_445" class="sc_form sc_form_style_form_2 margin_top_huge margin_bottom_huge">
                                            <h2 class="sc_form_title sc_item_title">Book Now</h2>
                                            <div class="sc_form_descr sc_item_descr">This is optional subheading</div>
                                            <form id="sc_form_445_form" data-formtype="form_2" method="post" action="includes/sendmail.php">
                                                <div class="sc_form_info">
                                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2">
                                                        <div class="column-1_2 sc_column_item">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="sc_form_item sc_form_field label_over">
                                                                        <i class="icon icon-user-light"></i>
                                                                        <label class="required" for="sc_form_username">Name</label>
                                                                        <input id="sc_form_username" type="text" name="username" placeholder="Name *">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="column-1_2 sc_column_item">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="sc_form_item sc_form_field label_over">
                                                                        <i class="icon icon-mobile-light"></i>
                                                                        <label class="required" for="sc_form_phone">Phone</label>
                                                                        <input id="sc_form_phone" type="text" name="phone" placeholder="Phone (Ex. +1-234-567-890)">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2">
                                                        <div class="column-1_2 sc_column_item">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="sc_form_item sc_form_field label_over">
                                                                        <i class="icon icon-mail-light"></i>
                                                                        <label class="required" for="sc_form_email">E-mail</label>
                                                                        <input id="sc_form_email" type="text" name="email" placeholder="E-mail *">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="column-1_2 sc_column_item">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="sc_form_item sc_form_field label_over">
                                                                        <i class="icon icon-dentrario_add_user"></i>
                                                                        <label class="required" for="sc_form_doctor">Doctor</label>
                                                                        <input id="sc_form_doctor" type="text" name="doctor" placeholder="Doctor">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sc_form_item sc_form_message label_over">
                                                    <label class="required" for="sc_form_message">Message</label>
                                                    <textarea id="sc_form_message" data-autoresize rows="1" name="message" placeholder="Message"></textarea>
                                                </div>
                                                <div class="sc_form_item sc_form_button">
                                                    <button class="aligncenter">Make an Appointment</button>
                                                </div>
                                                <div class="result sc_infobox"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <article class="myportfolio-container minimal-light" id="esg-grid-3-1-wrap">
                                        <div id="esg-grid-3-1" class="esg-grid">
                                            <ul>

                                                <?php $last_id = null; foreach ($image_list as $item) {
                                                    $last_id = $item->id;
                                                    ?>
                                                    <li id="eg-3-post-id-<?=$item->id?>" class="filterall filter-gallery eg-dentrario-wrapper eg-post-id-<?=$item->id?>" data-date="1452598735" data-cobblesw="<?=$item->ratio_x?>" data-cobblesh="<?=$item->ratio_y?>">
                                                        <div class="esg-media-cover-wrapper">
                                                            <div class="esg-entry-media">
                                                                <img src="<?=site_url().'upload/images/'.$item->thumbnail?>" alt="<?=$item->thumbnail_alt?>" width="910" height="676">
                                                            </div>
                                                            <div class="esg-entry-cover esg-fade" data-delay="0">
                                                                <div class="esg-overlay esg-fade eg-dentrario-container" data-delay="0"></div>
                                                                <div class="esg-center eg-post-857 eg-dentrario-element-0-a esg-falldown" data-delay="0.1">
                                                                    <a class="eg-dentrario-element-0 eg-post-857 esgbox" href="<?=site_url().'upload/images/'.$item->thumbnail?>" lgtitle="<?=$item->title?>" >
                                                                        <i class="eg-icon-search"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="esg-center eg-post-857 eg-dentrario-element-1-a esg-falldown" data-delay="0.2">
                                                                    <a class="eg-dentrario-element-1 eg-post-857" href="<?=site_url().'upload/images/'.$item->thumbnail?>" target="_self">
                                                                        <i class="eg-icon-link"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="esg-center eg-dentrario-element-8 esg-none esg-clear"></div>
                                                                <div class="esg-center eg-dentrario-element-9 esg-none esg-clear"></div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <?php
                                                } ?>


                                            </ul>
                                            <input type="hidden" id="last_id" value="<?=$last_id?>">

                                        </div>
                                    </article>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
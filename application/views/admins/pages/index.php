<?php echo $tab_language ?>
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon2-image-file"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Management Page <?php echo $row->title ?>
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<form method="post" action="<?php echo base_url('proweb/pages/update/'.$row->id); ?>" enctype="multipart/form-data"
			  class="form-send">
			<input type="hidden" name="type" value="<?php echo $type ?>">
			<div class="form-group">
				<label for="exampleSelect1">Title</label>
                <textarea class="form-control" name="title" placeholder="Title" ><?php echo $row->title ?></textarea>
            </div>
			<div class="form-group">
				<label for="exampleSelect1">Sub Title</label>
                <textarea class="form-control" name="title_sub" placeholder="Title Sub" ><?php echo $row->title_sub ?></textarea>
			</div>
			<div class="form-group" style="margin-left: 20px; margin-right: 20px">
				<label>Description</label>
				<textarea class="tinymce" id="exampleTextarea" rows="3" name="description"><?php echo $row->description ?></textarea>
			</div>
            <?php if($row->seo == 'yes') { ?>
			<div class="form-group">
				<label for="exampleSelect1">Meta title</label>
				<input type="text" class="form-control" value="<?php echo $row->meta_title ?>" name="meta_title">
			</div>
			<div class="form-group">
				<label for="exampleSelect1">Meta Description</label>
                <textarea class="form-control" name="meta_description" placeholder="Meta Description" ><?php echo $row->meta_description ?></textarea>
			</div>
			<div class="form-group">
				<label for="exampleSelect1">Meta Keywords</label>
				<input type="text" class="form-control" value="<?php echo $row->meta_keywords ?>" name="meta_keywords">
			</div>
            <?php } ?>

			<input type="submit" class="btn btn-primary" name="submit" value="Update Data">

		</form>

	</div>
</div>

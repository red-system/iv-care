<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon2-image-file"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Management Kontak Email
			</h3>
		</div>

	</div>
	<div class="kt-portlet__body">
		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable datatable">
			<thead>
			<tr>
				<th class="d-none"></th>
				<th width="20">No</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Phone</th>
                <th>Message</th>
                <th>Date</th>
				<th width="130">Option</th>
			</tr>
			</thead>
			<?php $no = 1 ?>
			<tbody>
			<?php foreach ($kontak as $datas) : ?>
				<tr>
					<td class="d-none data-row">
						<textarea><?php echo json_encode($datas) ?></textarea>
					</td>
					<td><?php echo $no ?></td>
					<td><?php echo $datas->nama ?></td>
					<td><?php echo $datas->email ?></td>
                    <td><?php echo $datas->phone ?></td>
                    <td><?php echo word_limiter($datas->message,15);?></td>
                    <td><?php echo $datas->created_at ?></td>
                    <td>
						<a href="#"
						   class="btn btn-success btn-elevate btn-elevate-air btn-edit" data-tinymce="true">Detail</a>
					</td>
				</tr>
				<?php $no++ ?>
			<?php endforeach; ?>
			</tbody>
		</table>
		<!--end: Datatable -->
	</div>
</div>
<!--begin::Modal-->


<form method="post" action="<?php echo base_url() . 'proweb/kontak_email/update'; ?>" enctype="multipart/form-data"
	  class="form-send">

	<div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">

		<input type="hidden" name="id">

		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Data Email</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="exampleSelect1">Nama</label>
						<input type="text" class="form-control" placeholder="Nama" name="nama" readonly>
					</div>
                    <div class="form-group">
						<label for="exampleSelect1">Email</label>
						<input type="text" class="form-control" placeholder="Email" name="email" readonly>
					</div>
					<div class="form-group">
						<label for="exampleSelect1">Phone</label>
						<input type="text" class="form-control" placeholder="Phone" name="phone" readonly>
					</div>
                    <div class="form-group">
						<label>Message</label>
						<textarea class="form-control" id="exampleTextarea" rows="10" name="message" readonly></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>



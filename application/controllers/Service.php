<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'service');

        $data = $this->main->data_front();
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/custom-style.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "js/vendor/comp/comp.min.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "css/core.messages.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/fcc8474e79.js");
        array_push($this->js, "js/vendor/modernizr.min.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/social-share.js");
        array_push($this->js, "js/custom/comment-reply.min.js");
        array_push($this->js, "js/custom/embed.min.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/custom/core.messages.js");
        array_push($this->js, "js/vendor/comp/comp_front.min.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/service');
        $this->load->view('user/statis/footer');

    }
    public function category($id = '')
    {

        $data = $this->main->data_front();
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/vendor/comp/comp_front.min.js");
        array_push($this->js, "js/custom/sweetalert2.all.min.js");
        array_push($this->js, "js/custom/service.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $this->session->unset_userdata(array('menu','menu_html'));

        $data['page'] = $this->db->where(array('id'=>$id))->get('layanan')->row();
        $this->session->set_userdata('menu', $data['page']->url_title);
        $data['captcha'] = $this->main->captcha();
        $this->load->view('user/statis/header',$data);
        $this->load->view('user/service');
        $this->load->view('user/statis/footer');
    }

}

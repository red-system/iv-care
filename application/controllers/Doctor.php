<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'doctor');

        $data = $this->main->data_front();
        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");



        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['page'] = $this->db->where('use', 'yes')->where('position','Doctor')->get('team')->result();

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/doctor');
        $this->load->view('user/statis/footer');

    }

}

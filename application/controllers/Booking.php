<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();
        array_push($this->css, "css/animations.css");
        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "js/vendor/booked/plugin.booked.css");
        array_push($this->css, "css/plugin.tribe-events.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/custom-style.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "js/vendor/comp/comp.min.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "css/core.messages.css");
        array_push($this->css, "js/vendor/magnific/magnific-popup.min.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/social-share.js");
        array_push($this->js, "js/custom/embed.min.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/custom/core.messages.js");
        array_push($this->js, "js/vendor/comp/comp_front.min.js");
        array_push($this->js, "http://maps.google.com/maps/api/js?key=");
        array_push($this->js, "js/vendor/core.googlemap.js");
        array_push($this->js, "js/vendor/magnific/jquery.magnific-popup.min.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;


        $this->load->view('user/statis/header',$data);
        $this->load->view('user/booking');
        $this->load->view('user/statis/footer');

    }

}

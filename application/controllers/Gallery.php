<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
        $this->load->model('m_gallery','',true);
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();


        array_push($this->css, "css/font.css");
        array_push($this->css, "js/vendor/esg/settings.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "js/vendor/esg/lightbox.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/gallery.js");
        array_push($this->js, "js/vendor/esg/lightbox.js");
        array_push($this->js, "js/vendor/esg/jquery.themepunch.tools.min.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/vendor/esg/jquery.themepunch.essential.min.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['image_list'] = $this->m_gallery->get_data_front();

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/gallery');
        $this->load->view('user/statis/footer');

    }
    function more(){
        $id = $this->input->post('token');
        $data =  $this->m_gallery->get_data_front($id);
        $test = '';
        foreach ($data as $item){
            $test .= '<li id="eg-3-post-id-'.$item->id.'" class="filterall filter-gallery eg-dentrario-wrapper eg-post-id-'.$item->id.' eg-newli" data-date="1452598806" data-cobblesw="'.$item->ratio_x.'" data-cobblesh="'.$item->ratio_y.'//">'.
    '<div class="esg-media-cover-wrapper">'.
        '<div class="esg-entry-media">'.
            '<img src="'.site_url().'/upload/images/'.$item->thumbnail.'" alt="" width="2400" height="1762">'.
        '</div>'.
        '<div class="esg-entry-cover esg-fade" data-delay="0">'.
            '<div class="esg-overlay esg-fade eg-dentrario-container" data-delay="0"></div>'.
            '<div class="esg-center eg-post-'.$item->id.'eg-dentrario-element-0-a esg-falldown" data-delay="0.1">'.
                '<a class="eg-dentrario-element-0 eg-post-'.$item->id.' esgbox" href="'.site_url().'/upload/images/'.$item->thumbnail.'" data-width="2400" data-height="1762">'.
                    '<i class="eg-icon-search"></i></a>'.
            '</div>'.
            '<div class="esg-center eg-post-'.$item->id.' eg-dentrario-element-1-a esg-falldown" data-delay="0.2">'.
                '<a class="eg-dentrario-element-1 eg-post-'.$item->id.'" href="'.site_url().'/upload/images/'.$item->thumbnail.'" target="_self"><i class="eg-icon-link"></i></a>'.
            '</div>'.
            '<div class="esg-center eg-dentrario-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>'.
            '<div class="esg-center eg-dentrario-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>'.
        '</div>'.
    '</div>'.
'</li>';
        }
        $result['success'] = true;
        $result['message'] = '';
        $result['data'] = $test;
        echo json_encode($result);

    }


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_news extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
        $this->load->model('m_blog','',true);
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();
        array_push($this->css, "css/animations.css");
        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/custom-style.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "css/core.messages.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/fcc8474e79.js");
        array_push($this->js, "js/vendor/modernizr.min.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/embed.min.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/custom/core.messages.js");
        array_push($this->js, "js/vendor/isotope/isotope.pkgd.min.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $this->load->library('pagination');
        $total_row = $this->m_blog->total_publish();
        $per_page = 6;
        $page = $this->uri->segment(3);
        $page = $page == '' ? 1 : $page;
        $offset =  ($page-1) * $per_page;

        $config['base_url'] = base_url().'blog-news/page/';
        $config['total_rows'] = $total_row;
        $config['per_page'] = $per_page;
        $config['cur_tag_open'] = '<span class="pager_current active">';
        $config['cur_tag_close'] = '</span>';
        $config['uri_segment'] = 3;
        $config['first_url'] = base_url().'blog-news/page/1';
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();
        $data['pagination'] = $pagination;
        $blogs = $this->m_blog->list_publish($per_page,$offset);
        foreach ($blogs as $item){
            $item->menu_url = base_url().'blog-news/'.$this->main->slug($item->title);
        }
        $data['blogs'] = $blogs;

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/blog_news');
        $this->load->view('user/statis/footer');

    }
    function detail($id){
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();

        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/shortcodes.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['pages'] = $this->m_blog->detail_blog($id);

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/blog_news_detail');
        $this->load->view('user/statis/footer');
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
        $this->load->helper('text');
        $this->load->model('m_blog','',true);
        $this->load->model('m_partner','',true);
        $this->load->model('m_faq','',true);
        $this->load->model('m_layanan','',true);
        $this->load->model('m_count','',true);
        $this->load->model('m_image_page','',true);
    }

    public function index()
    {

       // array_push($this->css, "css/beranda.min.css");

        array_push($this->css, "css/font.css");
        array_push($this->css, "js/vendor/revslider/settings.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "js/vendor/swiper/swiper.min.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/esg/jquery.themepunch.tools.min.js");
        array_push($this->js, "js/vendor/revslider/jquery.themepunch.revolution.min.js");
        array_push($this->js, "js/vendor/revslider/extensions/revolution.extension.slideanims.min.js");
        array_push($this->js, "js/vendor/revslider/extensions/revolution.extension.layeranimation.min.js");
        array_push($this->js, "js/vendor/revslider/extensions/revolution.extension.navigation.min.js");
        array_push($this->js, "js/vendor/revslider/extensions/revolution.extension.parallax.min.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/vendor/comp/comp_front.min.js");
        array_push($this->js, "js/vendor/swiper/swiper.min.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/vendor/isotope/isotope.pkgd.min.js");
        array_push($this->js, "js/vendor/ui/widget.min.js");
        array_push($this->js, "js/vendor/ui/accordion.min.js");
        $data =  $this->main->data_front();
        $data["css"] = $this->css;
        $data["js"] = $this->js;

        $data['sliders'] = $this->db->where('use', 'yes')->get('slider')->result();
        $data['testimony'] = $this->db->where('use', 'yes')->get('comment')->result();
        $data['team'] = $this->db->where('use', 'yes')->where('position','Doctor')->get('team')->result();

        $blogs = $this->m_blog->home_latest_news();
        foreach ($blogs as $item){
            $item->menu_url = base_url().'blog-news/'.$this->main->slug($item->title);
        }
        $data['blog'] =$blogs;
        $data['partner'] = $this->m_partner->get_data()->result();
        $layanan = $this->m_layanan->get_data()->result();
        foreach ($layanan as $item){
            $item->menu_url = base_url().'service/'.$this->main->slug($item->title);
        }
        $data['layanan'] = $layanan;
        $data['faq'] = $this->m_faq->get_data_home()->result();
        $data['count'] = $this->m_count->get_data()->result();
        $data['page_image'] = $this->m_image_page->get_data()->row()->thumbnail;
        $data['about_page'] = $this->db->where('type','profile')->get('pages')->row()->description;
        $data['testimony'] = $this->db->where('use', 'yes')->get('comment')->result();

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/home');
        $this->load->view('user/statis/footer');
    }

}

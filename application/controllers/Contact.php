<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
        $this->load->model('m_image_page','',true);
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();
        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");

        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "js/vendor/magnific/magnific-popup.min.css");
        array_push($this->css, "template_admin/vendors/general/bootstrap/dist/css/bootstrap.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/vendor/magnific/jquery.magnific-popup.min.js");
        array_push($this->js, "js/custom/sweetalert2.all.min.js");
        array_push($this->js, "js/custom/contact.js");



        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['page_image'] = $this->m_image_page->get_data()->row()->thumbnail;
        $data['captcha'] = $this->main->captcha();
        $this->load->view('user/statis/header',$data);
        $this->load->view('user/contact');
        $this->load->view('user/statis/footer');

    }
    public function captcha_check($str)
    {
        if ($str == $this->session->userdata('captcha_mwz')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('captcha_check', 'security code was wrong, please fill again truly');
            return FALSE;
        }
    }
    function send_message()
    {

        error_reporting(0);
        $this->load->model('m_kontak_email');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Fill form completly',
                'errors' => array(
                    'nama' => form_error('nama'),
                    'phone' => form_error('phone'),
                    'email' => form_error('email'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha'),
                )
            ));
        } else {
            $email_admin = $this->db->where('use', 'yes')->get('email')->result();
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');

            $data = array(
                'nama' => $nama,
                'email' => $email,
                'phone' => $phone,
                'message' => $message
            );
            $this->m_kontak_email->input_data($data);


            $message_admin = '

Dear Admin,<br /><br />
You have contact us message from web form<br />
form details as follows:<br /><br />

<table width="100%">
    <tr>
        <td width="80">Nama</td>
        <td width="920">: <strong>'. $nama .'</strong></td>
    </tr>
    <tr>
        <td width="80">Email</td>
        <td width="920">: <strong>'. $email .'</strong></td>
    </tr>
    <tr>
        <td width="80">No HP</td>
        <td width="920">: <strong>'. $phone .'</strong></td>
    </tr>
    <tr>
        <td width="80">Message</td>
        <td width="920">: <strong>'. $message .'</strong></td>
    </tr>
</table>
<br /><br />
Regarding,<br />
Contact Us System '.$this->main->web_name().'<br /><br />';

            foreach($email_admin as $r) {
//			    echo $r->email.', ';
                $this->main->mailer_auth('Kontak Kami - Website', $r->email, $this->main->web_name().' Administrator ', $message_admin);
            }

            echo json_encode(array(
                'status'=>'success',
                'message'=>'Email as already sent, please wait a moment for our reply ^_^'
            ));

        }

    }
}

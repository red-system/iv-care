<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends MY_Controller {

    function __construct(){
        parent:: __construct();
        $this->load->helper(array('form', 'url', 'html','language'));
        $this->load->model('m_layanan','',true);
        $this->load->model('m_image_page','',true);
    }

    public function index()
    {
        $this->session->unset_userdata(array('menu','menu_html'));
        $this->session->set_userdata('menu', 'about-us');

        $data = $this->main->data_front();
        array_push($this->css, "css/font.css");
        array_push($this->css, "css/fontello/css/fontello.css");
        array_push($this->css, "css/style.css");
        array_push($this->css, "css/core.animation.css");
        array_push($this->css, "css/shortcodes.css");
        array_push($this->css, "css/skin.css");
        array_push($this->css, "css/responsive.css");
        array_push($this->css, "css/skin.responsive.css");
        array_push($this->css, "css/custom.css");
        array_push($this->css, "js/vendor/swiper/swiper.min.css");

        array_push($this->js, "js/vendor/jquery/jquery.js");
        array_push($this->js, "js/vendor/jquery/jquery-migrate.min.js");
        array_push($this->js, "js/custom/custom.js");
        array_push($this->js, "js/vendor/jquery/core.min.js");
        array_push($this->js, "js/vendor/superfish.js");
        array_push($this->js, "js/custom/jquery.slidemenu.js");
        array_push($this->js, "js/custom/core.utils.js");
        array_push($this->js, "js/custom/core.init.js");
        array_push($this->js, "js/custom/init.js");
        array_push($this->js, "js/custom/shortcodes.js");
        array_push($this->js, "js/vendor/comp/comp_front.min.js");
        array_push($this->js, "js/vendor/swiper/swiper.min.js");

        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data['page_image'] = $this->m_image_page->get_data()->row()->thumbnail;
        $data['about_page'] = $this->db->where('type','profile')->get('pages')->row()->description;
        $data['layanan'] = $this->m_layanan->get_data()->result();
        $data['testimony'] = $this->db->where('use', 'yes')->get('comment')->result();

        $this->load->view('user/statis/header',$data);
        $this->load->view('user/about_us');
        $this->load->view('user/statis/footer');

    }

}

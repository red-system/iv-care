<?php

Class Main
{

    private $ci;
    private $web_name = 'IV Care Bali';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
                unlink($this->path_images . $filename);
            }
        }
    }
    function menu_layanan(){
         $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;
         $data = $this->ci->db
             ->select('id,title')
             ->where('id_language', $id_language)
             ->where('category', 'layanan')
             ->get('layanan')
             ->result();
         return $data;
    }
    function data_main()
    {
        $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'id_language' => $id_language,
        );

        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;

        return $data;
    }

    function data_front()
    {

        $lang_code = 'id'; //$this->ci->lang->lang();
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
        $footer = $this->ci->db->select('description')->where('type', 'footer')->get('pages')->row()->description;
//		$service_list = $this->ci->db->select('title,id')->where('id_language', $lang_active->id)->order_by('title', 'ASC')->get('category')->result();
//		$language_list = $this->ci->db->where('use','yes')->order_by('title','ASC')->get('language')->result();
        $menu_layanan = $this->menu_layanan();
        foreach ($menu_layanan as $item ){
            $item->menu_url = $this->slug($item->title);
        }
        $data = array(
            'favicon' => 'logo-favicon-red-consulting.png',
            'logo_header' => 'red-consulting-logo.png',
            'logo_footer' => 'red-consulting-logo-footer.png',
            'alamat' => 'Jl. Ratna No.68 G, Tonja, Kec. Denpasar Utara, Kota Denpasar, Bali 80239',
            'web_name' => 'IV Care Bali',
            'telephone' => '(0361) 4746102',
            'telephone_link' => 'tel:(0361) 4746102',
            'jam_kerja' => 'Mon - Fri : 08:00 - 17:00',
            'phone' => '',
            'whatsapp' => '',
            'whatsapp_link' => '',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:inforedconsulting69@gmail.com',
            'email' => 'inforedconsulting69@gmail.com',
            'facebook_link' => 'https://www.facebook.com/inforedconsulting/',
            'facebook_title' => 'Red Consulting',
            'instagram_link' => 'https://www.instagram.com/inforedconsulting/',
            'instagram_title' => 'Red Consulting',
            'linkedin_link' => 'https://www.linkedin.com/in/red-consulting-9aa740b5/',
            'linkedin_title' => 'Red Consulting',
            'view_secret' => FALSE,
            'author' => 'www.redconsulting.co.id/',
//			'services_list' => $service_list,
//			'language_list' => $language_list,
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'id_language' => $lang_active->id,
            'footer' => $footer,
            'menu_layanan' => $menu_layanan
        );

        return $data;
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('proweb/dashboard');
        }
    }

    function permalink($data)
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ','’',':', '/', '&', '\\', '\'', ',','(',')');
        $replace = array('-','-','-', '-', 'and', '-', '-', '-','','');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['max_width'] = 1920;
        $config['max_height'] = 1080;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'font_path'  => './assets/css/fonts/mvboli.ttf',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link) {
        switch($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=".$link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=".$link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=".$link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=".$link."&title=".$title."&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=".$title."&media=".$link."&description=";
                break;
            case "email":
                return "mailto:".$link."?&subject=".$title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body)
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.ivcarebali.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "no-reply@ivcarebali.com"; //user email
            $mail->Password = "oQUhDw=]%p_3"; //password email
            $mail->SetFrom("no-reply@ivcarebali.com", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('proweb/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Umum Beranda',
                            'route' => base_url('proweb/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'sesi_1' => array(
//                            'label' => 'Sesi 1',
//                            'route' => base_url('proweb/pages/type/home_sesi_1'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_2' => array(
//                            'label' => 'Sesi 2',
//                            'route' => base_url('proweb/pages/type/home_sesi_2'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_3' => array(
//                            'label' => 'Sesi 3',
//                            'route' => base_url('proweb/pages/type/home_sesi_3'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
						'home_slider' => array(
							'label' => 'Home Slider',
							'route' => base_url('proweb/home_slider'),
							'icon' => 'fab fa-asymmetrik'
						),
                    )
                ),
                'services' => array(
                    'label' => 'Layanan Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
//						'services_page' => array(
//							'label' => 'Services Page',
//							'route' => base_url('proweb/pages/type/services'),
//							'icon' => 'fab fa-asymmetrik'
//						),
                        'categories' => array(
                            'label' => 'Halaman Layanan Kami',
                            'route' => base_url('proweb/pages/type/services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services' => array(
                            'label' => 'Daftar Item Layanan',
                            'route' => base_url('proweb/layanan'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'services_gallery' => array(
//                            'label' => 'Services Gallery List',
//                            'route' => base_url('proweb/tour_gallery'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
                    )
                ),
                'testimonial' => array(
					'label' => 'Testimonial',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'testimonial_page' => array(
							'label' => 'Testimonial Page',
							'route' => base_url('proweb/pages/type/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
						'testimonial_list' => array(
							'label' => 'Testimonial List',
							'route' => base_url('proweb/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
                'partner' => array(
                    'label' => 'Partner Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'partner_page' => array(
                            'label' => 'Partner Page',
                            'route' => base_url('proweb/pages/type/partner'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'partner_list' => array(
                            'label' => 'Partner List',
                            'route' => base_url('proweb/partner'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'gallery_photo' => array(
                    'label' => 'Galeri Foto',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'gallery_photo_page' => array(
                            'label' => 'Halaman Galeri Foto',
                            'route' => base_url('proweb/pages/type/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'gallery_photo_list' => array(
                            'label' => 'Daftar Galeri Foto',
                            'route' => base_url('proweb/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
                'blog' => array(
                    'label' => 'Artikel',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halaman Artikel',
                            'route' => base_url('proweb/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_category' => array(
                            'label' => 'Kategori Artikel',
                            'route' => base_url('proweb/blog_category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Artikel',
                            'route' => base_url('proweb/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'profile' => array(
                    'label' => 'Tentang Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'profile_page' => array(
                            'label' => 'Halaman Tentang Kami',
                            'route' => base_url('proweb/pages/type/profile'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profile_image' => array(
                            'label' => 'Page Image',
                            'route' => base_url('proweb/image_page'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profil_sesi_0' => array(
                            'label' => 'Halaman Profile',
                            'route' => base_url('proweb/pages/type/profil_sesi_0'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profil_sesi_1' => array(
                            'label' => 'Halaman Offer',
                            'route' => base_url('proweb/pages/type/profil_sesi_1'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'offer' => array(
                            'label' => 'Daftar Item Offer',
                            'route' => base_url('proweb/offer'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profil_page_team' => array(
                            'label' => 'Halaman Team',
                            'route' => base_url('proweb/pages/type/profil_page_team'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profile_team' => array(
                            'label' => 'Daftar Team',
                            'route' => base_url('proweb/profile_team'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                        'profil_sesi_2' => array(
                            'label' => 'Halaman Count',
                            'route' => base_url('proweb/pages/type/profil_sesi_2'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'count' => array(
                            'label' => 'Daftar Count',
                            'route' => base_url('proweb/count'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),

//				'gallery_video' => array(
//					'label' => 'Gallery Video',
//					'route' => 'javascript:;',
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array(
//						'gallery_photo_page' => array(
//							'label' => 'Gallery Video Page',
//							'route' => base_url('proweb/pages/type/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'gallery_photo_list' => array(
//							'label' => 'Gallery Video List',
//							'route' => base_url('proweb/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//
//						),
//					)
//				),
//				'about_us' => array(
//					'label' => 'About Us',
//					'route' => base_url('proweb/pages/type/about_us'),
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array()
//				),

                'contact_us' => array(
					'label' => 'Kontak Kami',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'contact_us_page' => array(
							'label' => 'Halaman Kontak Kami',
							'route' => base_url('proweb/pages/type/contact_us'),
							'icon' => 'fab fa-asymmetrik'
						),
						'contact_us_list' => array(
							'label' => 'Email List',
							'route' => base_url('proweb/kontak_email'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
                'faq' => array(
					'label' => 'FAQ',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'faq_page' => array(
							'label' => 'FAQ Page',
							'route' => base_url('proweb/pages/type/faq'),
							'icon' => 'fab fa-asymmetrik'
						),
						'faq_list' => array(
							'label' => 'FAQ List',
							'route' => base_url('proweb/faq'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
                'syarat_ketentuan' => array(
                    'label' => 'Syarat Ketentuan',
                    'route' => base_url('proweb/pages/type/syarat_ketentuan'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'kebijakan_privasi' => array(
                    'label' => 'Kebijakan Privasi',
                    'route' => base_url('proweb/pages/type/kebijakan_privasi'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'reservation' => array(
//                    'label' => 'Reservation',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'reservation_page' => array(
//                            'label' => 'Reservation Page',
//                            'route' => base_url('proweb/pages/type/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'reservation_list' => array(
//                            'label' => 'Reservation List',
//                            'route' => base_url('proweb/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
                'footer' => array(
                    'label' => 'Footer Web',
                    'route' => base_url('proweb/pages/type/footer'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
            'OTHERS MENU' => array(
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('proweb/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'language' => array(
                    'label' => 'Language',
                    'route' => base_url('proweb/language'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('proweb/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}

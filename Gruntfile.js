module.exports = function(grunt) {

  var default_js = [
      'assets/js/vendor/jquery/jquery.js',
      'assets/js/vendor/jquery-migrate.min.js',
      'assets/js/custom/custom.js',
      'assets/js/vendor/jquery/core.min.js',
      'assets/js/vendor/superfish.js',
      'assets/js/custom/jquery.slidemenu.js',
      'assets/js/custom/core.utils.js',
      'assets/js/custom/core.init.js',
      'assets/js/custom/init.js'
  ];

  var default_css = [
      'assets/css/font.css',
      'assets/css/fontello/css/fontello.css',
      'assets/css/style.css',
      'assets/css/core.animation.css',
      'assets/css/shortcodes.css',
      'assets/css/skin.css',
      'assets/css/responsive.css',
      'assets/css/skin.responsive.css',
      'assets/css/custom.css'
  ];

  // Begin beranda

  var beranda_js = [
      'assets/js/vendor/esg/jquery.themepunch.tools.min.js',
      'assets/js/vendor/revslider/jquery.themepunch.revolution.min.js',
      'assets/js/vendor/revslider/extensions/revolution.extension.slideanims.min.js',
      'assets/js/vendor/revslider/revolution.extension.slideanims.min.js', 
      'assets/js/vendor/revslider/extensions/revolution.extension.layeranimation.min.js',
      'assets/js/vendor/revslider/extensions/revolution.extension.navigation.min.js',
      'assets/js/vendor/revslider/extensions/revolution.extension.parallax.min.js',
      'assets/js/vendor/comp/comp_front.min.js',
      'assets/js/vendor/swiper/swiper.min.js',
      'assets/js/vendor/isotope/isotope.pkgd.min.js',
      'assets/js/vendor/ui/widget.min.js',
      'assets/js/vendor/ui/accordion.min.js',
  ];
  beranda_js = default_js.concat(beranda_js);

  var beranda_css = [
        'assets/js/vendor/revslider/settings.css',
        'assets/js/vendor/swiper/swiper.min.css',
  ];
    beranda_css = default_css.concat(beranda_css);

  grunt.initConfig({
    jsDistDir: 'assets/js/',
    cssDistDir: 'assets/css/',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
            default_js: {
                options: {
                    separator: ';'
                },
                src: default_js,
                dest: '<%=jsDistDir%>default.js'
            },
            default_css: {
                src: default_css,
                dest: '<%=cssDistDir%>default.css'
            },
            beranda_js: {
                options: {
                    separator: ';'
                },
                src: beranda_js,
                dest: '<%=jsDistDir%>beranda.js'
            },
            beranda_css: {
                src: beranda_css,
                dest: '<%=cssDistDir%>beranda.css'
            },
        },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%=jsDistDir%>default.min.js': ['<%= concat.default_js.dest %>'],
          '<%=jsDistDir%>beranda.min.js': ['<%= concat.beranda_js.dest %>'],
        }
      }
    },
    cssmin: {
      add_banner: {
        options: {
          banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          '<%=cssDistDir%>default.min.css': ['<%= concat.default_css.dest %>'],
          '<%=cssDistDir%>beranda.min.css': ['<%= concat.beranda_css.dest %>'],
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
      'concat',
      'uglify',
      'cssmin'
  ]);

};
  
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2020 at 03:50 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rs_redconsulting`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `name`) VALUES
(2, 'mahendrawardana', '6001c26274f43ac7c6b2be2662a027f6', 'mahendra.adi.wardana@gmail.com', 'Mahendra Wardana'),
(32, 'agus', '5f4dcc3b5aa765d61d8327deb882cf99', 'agusdownload5293@gmail.com', 'Agus Arimbawa');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `id_team` int(11) DEFAULT NULL,
  `id_blog_category` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `thumbnail` text,
  `thumbnail_alt` text,
  `description` text NOT NULL,
  `use` enum('yes','no') DEFAULT 'no',
  `views` int(11) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `id_team`, `id_blog_category`, `title`, `url_title`, `thumbnail`, `thumbnail_alt`, `description`, `use`, `views`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`) VALUES
(1, 2, 1, 'Money Market Rates Finding the Best Accounts in 2016', 'Money-Market-Rates-Finding-the-Best-Accounts-in-2016', 'money-market-rates-finding-the-best-accounts-in-2016.jpg', 'Money Market Rates Finding the Best Accounts in 2016', '<p style="text-align: justify;">Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;Perspiciatis unde omnis iste natus sit volupt tem accusantium doloremque&nbsp;</p>', 'yes', 15, 'Money Market Rates Finding the Best Accounts in 2016', 'Money Market Rates Finding the Best Accounts in 2016', 'money, market', '2020-02-21 07:17:58'),
(2, 2, 1, 'Rates Finding the Best Accounts in 2017', 'Rates-Finding-the-Best-Accounts-in-2017', 'rates-finding-the-best-accounts-in-2017.jpg', 'Rates Finding the Best Accounts in 2017', '<p style="text-align: justify;">Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;</p>\r\n<p style="text-align: justify;">Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;Rates Finding the Best Accounts in 2017&nbsp;</p>', 'yes', 9, 'Rates Finding the Best Accounts in 2017 ', 'Rates Finding the Best Accounts in 2017. Rates Finding the Best Accounts in 2017 .', 'Rates', '2020-02-26 03:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `title`, `url_title`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `meta_keywords`, `meta_title`, `meta_description`) VALUES
(1, 'Perpajakan', 'Perpajakan', '<p>Pajak</p>', 'perpajakan1.png', 'Pajak', 'yes', 'pajak, artikel', 'Perpajakan', 'Artikel mengenai perpajakan'),
(2, 'Manajemen Laporan Keuagan', 'Manajemen-Laporan-Keuagan', '<p>Manajemen Laporan Keuangan</p>', 'manajemen-laporan-keuagan1.png', 'Manajemen Laporan Keuangan', 'yes', 'laporan keuangan, manajemen', 'Manajemen Laporan Keuangan', 'Manajemen Laporan Keuangan'),
(3, 'Finance Controller', 'Finance-Controller', '<p>Finance Controller</p>', 'finance-controller2.png', 'Finance Controller', 'yes', 'finance controller', 'Finance Controller', 'Finance Controller');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_sub` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `title`, `title_sub`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `date`) VALUES
(1, 'Lisa Larson', 'Company Director', 'Being a company''s director requires maximum attention and devotion. This was exactly what I felt when turned to your products and services. All our questions and inquiries were answered effectively and right away. Our website has never looked better, ever', 'lisa-larson.jpg', 'Lisa Larson', 'yes', ''),
(2, 'James Watson', 'Senior Manager', 'You never know what is gooing to happen until you try. But let me tell you that taking risk with these guys was totally worth it. Now we are a regular client, and this was probably the best decision we ever made! Our company appreciates your assistance an', 'james-watson.jpg', 'James Watson', 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `publish` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `count`
--

CREATE TABLE `count` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `sub_title` varchar(200) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `count`
--

INSERT INTO `count` (`id`, `title`, `sub_title`, `nilai`) VALUES
(2, 'KLIEN', 'Klien & Mitra yang Puas', 198),
(3, 'PARTNER', 'Partner', 30),
(4, 'PROYEK KONSULTING', 'PROYEK KONSULTING', 24);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `use` enum('yes','no') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `use`) VALUES
(1, 'agusdownload5293@gmail.com', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `type`) VALUES
(1, 'Tentang Kami', '', 'tentang-kami.jpg', 'Tentang Kami', 'yes', 'page_image'),
(2, 'Tentang Kami', '', 'tentang-kami1.jpg', 'Tentang Kami', 'yes', 'page_image');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `code` char(10) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `title`, `thumbnail`, `code`, `use`) VALUES
(1, 'English', 'english.png', 'en', 'no'),
(2, 'Indonesia', 'indonesia.png', 'id', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `layanan`
--

CREATE TABLE `layanan` (
  `id` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layanan`
--

INSERT INTO `layanan` (`id`, `id_language`, `category`, `title`, `url_title`, `title_sub`, `use`, `description`, `thumbnail`, `thumbnail_alt`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`) VALUES
(1, 2, 'layanan', 'Konsultan Pajak', 'Konsultan-Pajak', 'Kami dapat membantu dalam konsultasi perpajakan, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya.', 'yes', '<p align="justify">Upaya dalam melakukan penghematan pajak secara legal dapat dilakukan melalui manajemenpajak. Legalitas manajemen pajak tergantung dari instrumen yang dipakai yaitu setelah adaputusan Pengadilan. Manajemen pajak adalah sarana untuk memenuhi kewajiban perpajakan dengan benar, tetapi jumlah pajak yang dibayar dapat ditekan serendah mungkin untuk memperoleh laba dan likuiditas yang diharapkan.</p>\r\n<p align="justify">Maka dari kami RED Consulting hadir untuk membantu bisnis Anda. Kami akan menggunakan strategi yang efektif dan efisien agar perusahaan/bisnis mendapatkan laba dan likuiditas yang diharapkan.</p>', 'konsultan-pajak1.png', 'Konsultan Pajak', 'Konsultan Pajak', 'Kami dapat membantu dalam konsultasi perpajakan, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya.', 'konsultan pajak, perencanaan pajak, review kewajiban wajib pajak, pengurusan SPT Masa dan SPT Tahunan, penyelesaian masalah perpajakan, administrasi Pajak, dan lain sebagainya', '2020-02-17 08:53:04', '2020-02-25 08:31:35'),
(2, 2, 'layanan', 'Manajemen Laporan Keuangan', 'Manajemen-Laporan-Keuangan', 'Menyusun laporan keuangan sehingga kondisi perusahaan dapat terbaca dengan jelas, Bagian Laporan Keuangan yang disajikan meliputi, laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice.', 'yes', '<p align="justify">Upaya dalam melakukan penghematan pajak secara legal dapat dilakukan melalui manajemenpajak. Legalitas manajemen pajak tergantung dari instrumen yang dipakai yaitu setelah adaputusan Pengadilan. Manajemen pajak adalah sarana untuk memenuhi kewajiban perpajakan dengan benar, tetapi jumlah pajak yang dibayar dapat ditekan serendah mungkin untuk memperoleh laba dan likuiditas yang diharapkan.</p>\r\n<p align="justify">Maka dari kami RED Consulting hadir untuk membantu bisnis Anda. Kami akan menggunakan strategi yang efektif dan efisien agar perusahaan/bisnis mendapatkan laba dan likuiditas yang diharapkan.</p>', 'manajemen-laporan-keuangan.png', 'Manajemen Laporan Keuangan', 'Manajemen Laporan Keuangan', 'Menyusun laporan keuangan sehingga kondisi perusahaan dapat terbaca dengan jelas, Bagian Laporan Keuangan yang disajikan meliputi, laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice.', 'manajemen laporan keuangan,  laporan laba rugi, laporan neraca, laporan penyusutan & amortisasi, summery, advice', '2020-02-17 08:56:11', NULL),
(3, 2, 'layanan', 'Finance Controller', 'Finance-Controller', 'Menerapkan & Menyusun laporan Keuangan kepada manajemen sesuai dengan PSAK yang berlaku, Berkolaborasi dalam penyusunan inisiatif strategi, target perusahaan dan penyusunan anggaran keuangan untuk mencapai efektifitas operasional.', 'yes', '<p align="justify">Upaya dalam melakukan penghematan pajak secara legal dapat dilakukan melalui manajemenpajak. Legalitas manajemen pajak tergantung dari instrumen yang dipakai yaitu setelah adaputusan Pengadilan. Manajemen pajak adalah sarana untuk memenuhi kewajiban perpajakan dengan benar, tetapi jumlah pajak yang dibayar dapat ditekan serendah mungkin untuk memperoleh laba dan likuiditas yang diharapkan.</p>\r\n<p align="justify">Maka dari kami RED Consulting hadir untuk membantu bisnis Anda. Kami akan menggunakan strategi yang efektif dan efisien agar perusahaan/bisnis mendapatkan laba dan likuiditas yang diharapkan.</p>', 'finance-controller1.png', 'Finance Controller', 'Finance Controller', 'Menerapkan & Menyusun laporan Keuangan kepada manajemen sesuai dengan PSAK yang berlaku, Berkolaborasi dalam penyusunan inisiatif strategi, target perusahaan dan penyusunan anggaran keuangan untuk mencapai efektifitas operasional.', 'finance controller, menerapkan & menyusun laporan keuangan', '2020-02-17 08:58:47', NULL),
(4, 2, 'offer', 'Solusi Cerdas dan Cermat', '', '-', 'yes', '<p>Kami selalu memberikan solusi yang cerdas dan cermat dalam menyelesaikan setiap permasalahan perpajakan maupun keuangan.</p>', 'solusi-cerdas-dan-cermat.png', 'Solusi Cerdas dan Cermat', 'Solusi Cerdas dan Cermat', 'Kami selalu memberikan solusi yang cerdas dan cermat dalam menyelesaikan setiap permasalahan perpajakan maupun keuangan.', 'solusi cerdas dan cermat', '2020-02-21 09:32:53', '2020-02-21 09:34:54'),
(5, 2, 'offer', 'Responsible', '', '-', 'yes', '<p>Memberikan tanggung jawab kepada seluruh klien dalam pelayanan yang cerdas dan cermat.</p>', 'responsible.png', 'Responsible', 'Responsible', 'Memberikan tanggung jawab kepada seluruh klien dalam pelayanan yang cerdas dan cermat.', 'responsible', '2020-02-21 09:35:19', NULL),
(6, 2, 'offer', 'Sistem Kerja Inovatif', '', '-', 'yes', '<p>Kami memiliki system kerja yang inovatif dalam berkerjasama dengan klien sehingga kenyamanan dan keamanan klien bisa selalu terjaga dengan baik.</p>', 'sistem-kerja-inovatif.png', 'Sistem Kerja Inovatif', 'Sistem Kerja Inovatif', 'Kami memiliki system kerja yang inovatif dalam berkerjasama dengan klien sehingga kenyamanan dan keamanan klien bisa selalu terjaga dengan baik.', 'sistem kerja, inovatif, kenyamanan, keamanan', '2020-02-21 09:40:44', NULL),
(7, 2, 'offer', 'Education', '', '-', 'yes', '<p>Memberikan Edukasi manajemen keuangan dan bisnis sehingga dapat memberikan saran yang tepat dan akurat.</p>', 'education.png', 'Education', 'Education', 'Memberikan Edukasi manajemen keuangan dan bisnis sehingga dapat memberikan saran yang tepat dan akurat.', 'edukasi, manajemen kuangan dan bisnis', '2020-02-21 09:42:36', NULL),
(8, 2, 'offer', 'SDM (Sumber Daya Manusia) Berkualitas', '', '-', 'yes', 'Kami memiliki SDM yang cakap dan ahli dibidangnya masing-masing sehingga setiap permasalahan klien bisa tertangani dengan baik dan tepat.', 'sdm-sumber-daya-manusia-berkualitas.png', 'SDM (Sumber Daya Manusia) Berkualitas', 'SDM (Sumber Daya Manusia) Berkualitas', 'Kami memiliki SDM yang cakap dan ahli dibidangnya masing-masing sehingga setiap permasalahan klien bisa tertangani dengan baik dan tepat.', 'sdm berkualitas', '2020-02-21 09:44:23', NULL),
(9, 2, 'offer', 'Dedication', '', '-', 'yes', '<p>Kami berkomitmen memberikan seluruh energi kepada seluruh client, sehingga setiap permasalahan klien dapat tertangani dengan baik tanpa harus khawatir.</p>', 'dedication.png', 'Dedication', 'Dedication', 'Kami berkomitmen memberikan seluruh energi kepada seluruh client, sehingga setiap permasalahan klien dapat tertangani dengan baik tanpa harus khawatir.', 'Dedication', '2020-02-21 09:45:42', NULL),
(10, 2, 'faq', 'Bagaimana prosedurnya untuk bergabung dengan perusahaan Anda?', '', NULL, 'yes', '<p>Pembina utama kebahagiaan manusia. Tidak ada yang menolak, tidak suka, atau menghindari kesenangan itu sendiri, karena itu adalah kesenangan, tetapi karena mereka yang tidak tahu bagaimana mengejar kesenangan.</p>', '', '', '', '', '', NULL, '2020-02-26 13:51:15'),
(11, 2, 'faq', 'Ada pertanyaan ?', '', NULL, 'yes', '<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '', '', '', '', '', '2020-02-26 13:51:22', '2020-02-26 13:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `seo` enum('yes','no') DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `id_language`, `title`, `title_sub`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `type`, `seo`) VALUES
(1, 2, 'Testimonial', 'Kami sudah mendapatkan banyak testimonial dari klien / partner kami untuk membagikan pengalaman mereka saat melakukan kerjasama dengan kami.', '<p>te</p>', 'Testimonial', 'testimonial, klien, partner, kerjasama', 'Kami sudah mendapatkan banyak testimonial dari klien / partner kami untuk membagikan pengalaman mereka saat melakukan kerjasama dengan kami.', 'testimonial', 'yes'),
(2, 2, 'Artikel', 'Baca artkel terbaru dari kami yang meliputi bidang perpajakan dan keuangan.', '<p>-</p>', 'Artikel', 'artikel, pajak, keuangan, konsulting', 'Memberikan informasi berupa artikel yang berkaitan dengan pajak, keuangan, tips-tips dalam keuangan dan pajak.', 'blog', 'yes'),
(3, 2, 'Selamat Datang di <span class="color-active"> RED </span> Consulting', 'Kami ada untuk memberikan pelayanan yang terbaik untuk anda.', '<p align="justify">Derasnya persaingan bisnis yang nyaris tanpa kompromi, membuat management bisnis dan keuangan yang tepat dan efektif seakan menjadi sesuatu yang wajib dan harus diberikan ruang istimewa dalam sebuah perusahaan atau bisnis. Atas dasar itulah <span class="color-active">RED</span> Consulting hadir untuk menjadi solusi terbaik dalam menghadirkan manajemen bisnis yang tepat dan efektif untuk perusahaan atau bisnis anda sehingga perusahaan atau bisnis anda bisa bertumbuh dengan kuat dan stabil.</p>\r\n<p align="justify"><span class="color-active">RED</span> Consulting adalah perusahaan Konsultan Pajak dan Keuangan yang bernaung di bawah PT. Guna Artha Kencana dan berkantor di Jalan Ratna No. 68 G, Denpasa-Bali.</p>\r\n<p align="justify">Didukung oleh tim bisnis yang telah berpengalaman lebih dari 10 tahun, <span class="color-active">RED</span> Consulting siap untuk menjadi pilihan terbaik bagi pelaku usaha (individu atau perusahaan) yang ingin menjadikan manajemen bisnis dan keuangan yang tepat dan efektif sebagai pondasi dasar yang kuat dalam berbisnis.</p>', '', '', '', 'profil_sesi_0', 'yes'),
(4, 2, 'Layanan Utama Kami', 'Layanan terbaik kami yang dapat menjadi solusi perusahaan anda', '<p>-</p>', NULL, NULL, NULL, 'services', 'yes'),
(5, 2, '-', '-', '<p>R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan. Kami melayani perusahaan di berbagai bidang industri dengan mengedepankan layanan</p>', NULL, NULL, NULL, 'footer', 'yes'),
(6, 2, 'Hubungi Kami', 'Kami ada untuk memberikan pelayanan yang terbaik untuk Anda dan akan selalu siap membalas email yang Anda kirimkan dengan cepat.', '<p>-</p>', NULL, NULL, NULL, 'contact_us', 'yes'),
(7, 2, 'Mengapa memilih Kami ?', 'Kelebihan Kami', '<p>-</p>', NULL, NULL, NULL, 'profil_sesi_1', 'yes'),
(8, 2, 'Kami selalu di depan', 'Solusi Profesional untuk Bisnis Anda', '<p>-</p>', NULL, NULL, NULL, 'profil_sesi_2', 'yes'),
(9, 2, 'Red Consulting', '-', 'ts', 'RED Consulting', 'consulting, keuangan, perpajakan, perusahaan, bisnis', 'R.E.D. CONSULTING hadir untuk membantu perusahaan dengan membuka seluruh potensi  yang dimiliki untuk tumbuh lebih besar, dengan turut menghadirkan solusi terbaik dalam memenuhi penataan keuangan perusahaan.', 'home', 'yes'),
(10, 2, 'Galeri', 'Dalam galeri ini terdapat tips-tips untuk memajukan bisnis / perusahaan. Serta hasil dokumentasi yang dapat kami abadikan bersama partner / klien kami sewaktu kami sedang meeting dan tanda tangan kerjasama.', '<p>-</p>', 'Galeri', 'tips-tips, meeting', 'Dalam galeri ini terdapat tips-tips untuk memajukan bisnis / perusahaan. Serta hasil dokumentasi yang dapat kami abadikan bersama partner / klien kami sewaktu kami sedang meeting dan tanda tangan kerjasama.', 'gallery_photo', 'yes'),
(11, 2, 'Tentang Kami', '-', '<p>-</p>', 'Tentang Kami', 'consulting, konsultan pajak, konsultan keuangan, manajemen bisnis ', 'RED Consulting adalah perusahaan Konsultan Pajak dan Keuangan yang bernaung di bawah PT. Guna Artha Kencana. RED Consulting hadir untuk menjadi solusi terbaik dalam menghadirkan manajemen bisnis yang tepat dan efektif untuk perusahaan atau bisnis anda seh', 'profile', 'yes'),
(12, 2, 'Staff Kami', 'Kami berupaya agar bisnis Anda mulai bekerja secara efektif untuk Anda.', '<p>-</p>', NULL, NULL, NULL, 'profil_page_team', 'yes'),
(13, 2, 'Syarat & Ketentuan', 'Syarat dan ketentuan berikut adalah ketentuan dalam pengunjungan Situs, isi dan/atau konten, layanan, serta fitur lainnya yang ada di website ini.', '<h4 class="text-theme"><strong>1. Description of Service</strong></h4>\r\n<p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi at fuga alias harum quo quibusdam odit eum reprehenderit consectetur suscipit!</p>\r\n<h4 class="text-theme mt-5"><strong>2. Your Registration Obligations</strong></h4>\r\n<p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus.</p>\r\n<h4 class="text-theme mt-5"><strong>3. User Account, Password, and Security</strong></h4>\r\n<p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi.</p>\r\n<h4 class="text-theme mt-5"><strong>4. User Conduct</strong></h4>\r\n<p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium vel recusandae ad impedit ipsum, vitae facere expedita! Voluptatem iure dolorem dignissimos nisi magni a dolore, et inventore optio, voluptas, obcaecati.</p>', 'Syarat & Ketentuan', 'syarat, ketetuan', 'Syarat dan ketentuan berikut adalah ketentuan dalam pengunjungan Situs, isi dan/atau konten, layanan, serta fitur lainnya yang ada di website ini.', 'syarat_ketentuan', 'yes'),
(14, 2, 'Kebijakan Privasi', 'Kebijakan Privasi ini bertujuan meberikan informasi terkait komitmen kami dalam menjaga kerahasiaan seluruh data / informasi klien.', '<p align="justify">Seluruh informasi yang diberikan kepada <span class="color-active">RED</span> Consulting merupakan <span class="color-active">informasi rahasia</span> dan <span class="color-active">RED</span> Consulting berkomitmen dalam menjaga kerahasiaan tersebut.</p>\r\n<p align="justify"><span class="color-active">RED</span> Consulting menyadari seluruh informasi baik dari klien serta laporan / proyek yang sedang dikembangkan ataupun telah dihasilkan adalah <span class="color-active">bersifat sensitif,</span> dan hanya ditujukan untuk <span class="color-active">kepentingan klien</span> <span class="color-active">RED</span> Consuting.</p>\r\n<p align="justify">Seluruh tim <span class="color-active">RED</span> Consulting yang terlibat dalam pengikatan kerjasama dengan klien memiliki <span class="color-active">komitmen untuk menjaga kerahasiaan</span> dari klien <span class="color-active">RED</span> Consulting.</p>', 'Kebijakan Privasi', 'rahasia, kebijakan privasi, komitmen', 'Kebijakan Privasi ini bertujuan meberikan informasi terkait komitmen kami dalam menjaga kerahasiaan seluruh data / informasi klien.', 'kebijakan_privasi', 'yes'),
(15, 2, 'FAQ', 'Berikut ini adalah pertanyaan yang sering diajukan orang dan jawaban yang sudah disediakan.', '<p>-</p>', 'FAQ', 'faq', 'Berikut ini adalah pertanyaan yang sering diajukan orang dan jawaban yang sudah disediakan.', 'faq', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(50) NOT NULL,
  `address` text,
  `country` varchar(255) NOT NULL,
  `tour_start` varchar(255) NOT NULL,
  `total_adult` int(50) NOT NULL,
  `total_children` int(50) NOT NULL,
  `message` int(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `use` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `id_language`, `title`, `description`, `thumbnail`, `thumbnail_alt`, `url`, `use`) VALUES
(1, 2, ' Memberikan yang terbaik<br> dalam Konsultasi', 'Dengan memiliki tim yang berpengalaman dibidangnya<br> anda mendapatkan solusi yang terbaik dan tepat.', '-memberikan-yang-terbaikbr-dalam-konsultasi.jpg', 'Mendapatkan solusi yang terbaik dan tepat dalam masalah anda.', '-', 'yes'),
(2, 2, 'Menyelesaikan setiap permasalahan<br>perusahaan atau bisnis', 'Membantu dalam pengelolaan manajemen keuangan<br> hingga permasalahan perpajakan.', 'menyelesaikan-setiap-permasalahanbrperusahaan-atau-bisnis.jpg', 'Membantu dalam pengelolaan manajemen keuangan hingga permasalahan perpajakan', '-', 'yes'),
(3, 2, 'Dukungan bisnis terbaik<br>untuk Anda dari Kami', 'Dukungan yang terbaik untuk anda dalam pengelolaan perpajakan<br>maupun manajemen keuangan perusahaan atau bisnis anda.', 'dukungan-bisnis-terbaikbruntuk-anda-dari-kami.jpg', 'Dukungan bisnis terbaik untuk Anda dari Kami', '-', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `sub_layanan`
--

CREATE TABLE `sub_layanan` (
  `id` int(11) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `id_layanan` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_layanan`
--

INSERT INTO `sub_layanan` (`id`, `language_id`, `id_layanan`, `title`, `url_title`, `title_sub`, `use`, `description`, `thumbnail`, `thumbnail_alt`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 'Pendampingan Restitusi Pajak', 'Pendampingan-Restitusi-Pajak', '-', 'yes', '<p align="justify">Sering terjadi wajib pajak kelebihan melakukan pembayaran pajak. Bisa karena salah hitung nilai pajak atau salah nominal saat melakukan pembayaran pajak. Ada 2 (dua) hal yang bisa dilakukan untuk menyelesaikan permasalahan tersebut yakni dengan melakukan Restitusi (pengembalian) atau Pbk (pemindahbukuan). Jika jumlahnya besar tentunya Restitusi pajak adalah hal yang paling tepat untuk dilaksanakan.</p>\r\n<p align="justify">Kami siap mendampingi Anda untuk melakukan proses restitusi pajak dengan cermat dan tepat sehingga hak Anda atas pengembalian kelebihan pembayaran pajak tersebut bisa terlaksana sesuai dengan peraturan perpajakan yang berlaku di Indonesia.</p>', 'pendampingan-restitusi-pajak8.jpeg', 'Pendampingan Restitusi Pajak', 'Pendampingan Restitusi Pajak', 'Kami siap mendampingi Anda untuk melakukan proses restitusi pajak dengan cermat dan tepat sehingga hak Anda atas pengembalian kelebihan pembayaran pajak tersebut bisa terlaksana sesuai dengan peraturan perpajakan yang berlaku di Indonesia.', 'pendampingan, restitusi, pajak', '2020-02-21', NULL),
(3, 2, 1, 'Pendampingan Pemeriksaan Pajak', 'Pendampingan-Pemeriksaan-Pajak', '-', 'yes', '<p align="justify">Sering terjadi wajib pajak kelebihan melakukan pembayaran pajak. Bisa karena salah hitung nilai pajak atau salah nominal saat melakukan pembayaran pajak. Ada 2 (dua) hal yang bisa dilakukan untuk menyelesaikan permasalahan tersebut yakni dengan melakukan Restitusi (pengembalian) atau Pbk (pemindahbukuan). Jika jumlahnya besar tentunya Restitusi pajak adalah hal yang paling tepat untuk dilaksanakan.</p>\r\n<p align="justify">Kami siap mendampingi Anda untuk melakukan proses restitusi pajak dengan cermat dan tepat sehingga hak Anda atas pengembalian kelebihan pembayaran pajak tersebut bisa terlaksana sesuai dengan peraturan perpajakan yang berlaku di Indonesia.</p>', 'pendampingan-pemeriksaan-pajak.jpeg', 'Pendampingan Pemeriksaan Pajak', 'Pendampingan Pemeriksaan Pajak', 'Kami siap mendampingi Anda untuk melakukan proses restitusi pajak dengan cermat dan tepat sehingga hak Anda atas pengembalian kelebihan pembayaran pajak tersebut bisa terlaksana sesuai dengan peraturan perpajakan yang berlaku di Indonesia.', 'pendampingan, pemeriksaan, pajak', '2020-02-21', NULL),
(4, 2, 1, 'Penyusunan Laporan Perpajakan', 'Penyusunan-Laporan-Perpajakan', '-', 'yes', '<p>Sebagai pengusaha baik itu berbentuk orang pribadi ataupun perusahaan, ada 2 (dua) jenis.</p>', 'penyusunan-laporan-perpajakan.jpeg', 'Penyusunan Laporan Perpajakan', 'Penyusunan Laporan Perpajakan', 'Sebagai pengusaha baik itu berbentuk orang pribadi ataupun perusahaan, ada 2 (dua) jenis.', 'penyusunan, laporan, perpajakan', '2020-02-21', NULL),
(5, 2, 1, 'Pendampingan Administrasi Perpajakan', 'Pendampingan-Administrasi-Perpajakan', '-', 'yes', '<p>Untuk bisa menjalankan kewajiban perpajakan di Indonesia dengan benar dan tepat.</p>', 'pendampingan-administrasi-perpajakan.jpeg', 'Pendampingan Administrasi Perpajakan', 'Pendampingan Administrasi Perpajakan', 'Untuk bisa menjalankan kewajiban perpajakan di Indonesia dengan benar dan tepat.', 'pendampingan, administrasi, perpajakan', '2020-02-21', NULL),
(6, 2, 2, 'Penyusunan Laporan Keuangan', 'Penyusunan-Laporan-Keuangan', '-', 'yes', '<p align="justify">Laporan Keuangan adalah media tertulis yang berisikan laporan dan pembukuan dari seluruh kegiatan keuangan perusahaan pada suatu periode tertentu. Fungsi dari keberadaan Laporan Keuangan adalah untuk memberikan gambaran yang lengkap dari aktivitas keuangan perusahaan Anda meliputi arus pendapatan, pengeluaran, kegiatan investasi, pendanaan dari pihak ketiga, sampai dengan kebijakan pembagian deviden (bagi hasil) perusahaan. Secara umum, Laporan Keuangan akan meliputi 4 jenis laporan yakni Laporan Laba/Rugi, Laporan Neraca, Laporan Arus Kas dan Laporan Perubahan Modal.</p>\r\n<p align="justify">Kami memberi perhatian penting terhadap keberadaan Laporan Keuangan perusahaan Anda, mengingat peran pentingnya sebagai media pengambilan keputusan manajemen dan berhubungan dengan pihak ketiga. Proses penyusunan Laporan Keuangan akan didasarkan pada&nbsp;<em>best practice</em>&nbsp;dan standar akuntansi keuangan yang berlaku di Indonesia.</p>', 'penyusunan-laporan-keuangan.jpeg', 'Penyusunan Laporan Keuangan', 'Penyusunan Laporan Keuangan', 'Kami memberi perhatian penting terhadap keberadaan Laporan Keuangan perusahaan Anda, mengingat peran pentingnya sebagai media pengambilan keputusan manajemen dan berhubungan dengan pihak ketiga. ', 'penyusunan, laporan, keuangan', '2020-02-21', NULL),
(7, 2, 2, 'Menyusun Proyeksi Keuangan', 'Menyusun-Proyeksi-Keuangan', '-', 'yes', '<p>Menyusun proyeksi keuangan merupakan kegiatan penting.</p>', 'menyusun-proyeksi-keuangan.jpeg', 'Menyusun Proyeksi Keuangan', 'Menyusun Proyeksi Keuangan', 'Menyusun proyeksi keuangan merupakan kegiatan penting.', 'Menyusun Proyeksi Keuangan', '2020-02-21', NULL),
(8, 2, 3, 'Monitoring Anggaran Perusahaan', 'Monitoring-Anggaran-Perusahaan', '-', 'yes', '<p align="justify">roses monitoring anggaran/budget perusahaan merupakan proses yang penting untuk dilakukan pasca penyusunan anggaran telah dilakukan. Hal ini penting dilakukan agar perusahaan Anda terhindar dari pengeluaran-pengeluaran yang tidak sesuai dengan rencana awal dan dapat berpotensi menekan keuntungan perusahaan. Disamping itu, monitoring dilakukan untuk menjaga efektivitas beban keuangan perusahaan yang telah dikeluarkan. Secara umum, kegiatan monitoring dilakukan secara rutin pada periode tertentu, misalnya bulanan atau triwulanan, dan membandingkannya antara Realisasi Anggaran dengan dengan Anggaran awal yang telah disusun.</p>\r\n<p align="justify">Kami sebagai mitra bisnis jangka panjang Anda, berkomitmen membantu Anda dalam melakukan monitoring anggaran perusahaan. Kami memberi perhatian penting terhadap kesuksesan dan aspirasi pertumbuhan bisnis Anda.</p>\r\n<div class="single_footer_info">&nbsp;</div>', 'monitoring-anggaran-perusahaan.jpeg', 'Monitoring Anggaran Perusahaan', 'Monitoring Anggaran Perusahaan', 'Kami sebagai mitra bisnis jangka panjang Anda, berkomitmen membantu Anda dalam melakukan monitoring anggaran perusahaan. Kami memberi perhatian penting terhadap kesuksesan dan aspirasi pertumbuhan bisnis Anda.', 'monitoring, anggaran, perusahaan', '2020-02-21', NULL),
(9, 2, 3, 'Menyusun Anggaran Perusahaan', 'Menyusun-Anggaran-Perusahaan', '-', 'yes', '<p>Perencanaan anggaran atau biasa disebut dengan budgeting.</p>', 'menyusun-anggaran-perusahaan.jpeg', 'Menyusun Anggaran Perusahaan', 'Menyusun Anggaran Perusahaan', 'Perencanaan anggaran atau biasa disebut dengan budgeting.', 'menyusun, anggaran, perusahaan', '2020-02-21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `description` text,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `title`, `position`, `description`, `thumbnail`, `thumbnail_alt`, `use`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'I Made Dwi Harmana SE., M.Si., BKP', 'Tax Consultant (No Izin Praktek : KEP-3580/IP.B/PJ/2018)', '<p>I Made Dwi Harmana SE., M.Si., BKP menjabat sebagai Tax Consultant (No Izin Praktek : KEP-3580/IP.B/PJ/2018)</p>', 'i-made-dwi-harmana-se.--m.si.--bkp', NULL, 'yes', 'I Made Dwi Harmana SE., M.Si., BKP', 'I Made Dwi Harmana SE., M.Si., BKP', 'I Made Dwi Harmana SE., M.Si., BKP'),
(2, 'A.A Sagung Alit Dwijayanti', 'Tax & Accounting Manager', '<p>A.A Sagung Alit Dwijayanti menjabat sebagai Tax & Accounting Manager</p>', 'a.a-sagung-alit-dwijayanti', NULL, 'yes', 'A.A Sagung Alit Dwijayanti', 'A.A Sagung Alit Dwijayanti', 'A.A Sagung Alit Dwijayanti'),
(3, 'Ni Kadek Miantari', 'Tax Officer', '<p>Ni Kadek Miantari menjabat sebagai Tax Officer</p>', 'ni-kadek-miantari.jpg', NULL, 'yes', 'Ni Kadek Miantari', 'Ni Kadek Miantari', 'Ni Kadek Miantari'),
(4, 'I Gede Wahyu Indrawan', 'Tax Officer', '<p>I Gede Wahyu Indrawan menjabat sebagai Tax Officer</p>', 'i-gede-wahyu-indrawan.jpg', NULL, 'yes', 'I Gede Wahyu Indrawan', 'I Gede Wahyu Indrawan', 'I Gede Wahyu Indrawan'),
(5, 'Ni Putu Juliastari', 'Tax Officer', '<p>Ni Putu Juliastari menjabat sebagai Tax Officer</p>', 'ni-putu-juliastari.jpg', NULL, 'yes', 'Ni Putu Juliastari', 'Ni Putu Juliastari', 'Ni Putu Juliastari');

-- --------------------------------------------------------

--
-- Table structure for table `tour_gallery`
--

CREATE TABLE `tour_gallery` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `count`
--
ALTER TABLE `count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `layanan`
--
ALTER TABLE `layanan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sub_layanan`
--
ALTER TABLE `sub_layanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tour_gallery`
--
ALTER TABLE `tour_gallery`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `count`
--
ALTER TABLE `count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `layanan`
--
ALTER TABLE `layanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_layanan`
--
ALTER TABLE `sub_layanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tour_gallery`
--
ALTER TABLE `tour_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
